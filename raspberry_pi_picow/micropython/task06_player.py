from machine import UART, Pin
import time
import DFRobotDFPlayerMini

led = Pin("LED", Pin.OUT)

def printDetail(type: int, value: int):
    if type == DFRobotDFPlayerMini.TimeOut:
        print("Time Out!")
    elif type == DFRobotDFPlayerMini.WrongStack:
        print("Stack Wrong!")
    elif type == DFRobotDFPlayerMini.DFPlayerCardInserted:
        print("Card Inserted!")
    elif type == DFRobotDFPlayerMini.DFPlayerCardRemoved:
        print("Card Removed!")
    elif type == DFRobotDFPlayerMini.DFPlayerCardOnline:
        print("Card Online!")
    elif type == DFRobotDFPlayerMini.DFPlayerUSBInserted:
        print("USB Inserted!")
    elif type == DFRobotDFPlayerMini.DFPlayerUSBRemoved:
        print("USB Removed!")
    elif type == DFRobotDFPlayerMini.DFPlayerPlayFinished:
        print("Number: " + value, end = ' ')
        print("Play Finished!")
    if type == DFRobotDFPlayerMini.DFPlayerError:
        print("DFPlayerError:", end = ' ')
        if value == DFRobotDFPlayerMini.Busy:
            print("Card not found")
        elif type == DFRobotDFPlayerMini.Sleeping:
            print("Sleeping")
        elif type == DFRobotDFPlayerMini.SerialWrongStack:
            print("Get Wrong Stack")
        elif type == DFRobotDFPlayerMini.CheckSumNotMatch:
            print("Check Sum Not Match")
        elif type == DFRobotDFPlayerMini.FileIndexOut:
            print("File Index Out of Bound")
        elif type == DFRobotDFPlayerMini.FileMismatch:
            print("Cannot Find File")
        elif type == DFRobotDFPlayerMini.Advertise:
            print("In Advertise")

def dfr_player_mini_setup(player: DFRobotDFPlayerMini.DFRPlayerMini):

    print("DFRobot DFPlayer Mini Demo")
    print("Initializing DFPlayer ... (May take 3~5 seconds)")

    # Use serial to communicate with mp3.
    if player.begin() != True:
        print("Unable to begin:")
        print("    1.Please recheck the connection!")
        print("    2.Please insert the SD card!")
        return False

    print("DFPlayer Mini online.")
    # Set volume value. From 0 to 30
    player.volume(10)
    # Play the first mp3
    player.play(1)
    return True

def dfr_player_mini_loop(player: DFRobotDFPlayerMini.DFRPlayerMini):
    timer = time.ticks_ms()

    if (time.ticks_ms() - timer > 8000):
        timer = time.ticks_ms()
        # Play next mp3 every 8 second
        player.next()

    if (player.available()):
        # Print the detail message from DFPlayer to handle different errors and states.
        printDetail(player.readType(), player.read())

def dfr_test():
    uart0 = UART(0, baudrate=9600, tx=Pin(0), rx=Pin(1))
    player = DFRobotDFPlayerMini.DFRPlayerMini(uart0)
    if dfr_player_mini_setup(player):
        while True:
            time.sleep_ms(500)
            led.toggle()
            dfr_player_mini_loop(player)

testBytes = b'\x00'
print(testBytes)
print(type(hex(testBytes[0])))
print(hex(testBytes[0]))
date = int.from_bytes(testBytes, 'little')
print(type(date))
print(date)
dfr_test()
