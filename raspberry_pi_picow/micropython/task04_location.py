from machine import UART, Pin
import time
from micropyGPS import MicropyGPS


uart0 = UART(0, baudrate=9600, tx=Pin(0), rx=Pin(1))
my_gps = MicropyGPS()

while True:
    if uart0.any():
        if my_gps.update(uart0.read(1).decode("ascii")):
            print("Latitude:", my_gps.latitude_string())
            print("Longitude:", my_gps.longitude_string())
            print("Speed:", my_gps.speed_string("kph"), "or", my_gps.speed_string("mph"), "or", my_gps.speed_string("knot"))
            print("Date (Long Format):", my_gps.date_string("long"))
            print("Date (Short D/M/Y Format):", my_gps.date_string("s_dmy"))
            print("Date (Short M/D/Y Format):", my_gps.date_string("s_mdy"))
            time.sleep(5)
