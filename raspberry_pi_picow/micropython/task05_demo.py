#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from machine import Pin, I2C, RTC, Timer, UART
import time
import utime
import network
import ntptime
import urequests
import ujson
import uzlib
from micropyGPS import MicropyGPS
from ssd1306 import SSD1306_I2C
import ufont
import weather_ico
import framebuf


# gps status
GPS_STATUS_NONE = 0
GPS_STATUS_INIT = 1
GPS_STATUS_IDLE = 2
GPS_STATUS_SYNC = 3
GPS_STATUS_UPDATE = 4


WIFI_SSID = "asr-guest"  # "your-ssid"
WIFI_PASSWORD = "asr123456"  # "your-password"
QWEATHER_KEY = "e20da8dc347d477ca13aabdf522caa0c"
UTC_OFFSET = 8 * 60 * 60
WEEK = ['一', '二', '三', '四', '五', '六', '日']

g_tick_counter = 1
g_gps_status = GPS_STATUS_NONE
g_longi = "121.53"
g_lati = "31.17"


led = Pin("LED", Pin.OUT)
beep = Pin(16, Pin.OUT)
timer = Timer()
rtc = RTC()
i2c_oled = I2C(0, scl=Pin(9), sda=Pin(8), freq=400000)
display = SSD1306_I2C(128, 64, i2c_oled)
display.clear()
font = ufont.BMFont("unifont-14-12917-16.v3.bmf")
uart_gps = UART(0, baudrate=9600, tx=Pin(0), rx=Pin(1))
gps = MicropyGPS(location_formatting='dd')
wlan = network.WLAN(network.STA_IF)


def gps_process():
    if uart_gps.any():
        if gps.update(uart_gps.read(1).decode("ascii")):
            # print("Latitude:", gps.latitude_string())
            # print("Longitude:", gps.longitude_string())
            # print("Speed:", gps.speed_string("kph"), "or", gps.speed_string("mph"), "or", gps.speed_string("knot"))
            # # print("Date (Long Format):", gps.date_string("long"))
            # # print("Date (Short D/M/Y Format):", gps.date_string("s_dmy"))
            # print("Date (Short M/D/Y Format):", gps.date_string("s_mdy"))
            global g_gps_status, g_longi, g_lati
            if gps.longitude[0] != 0.0:
                if g_gps_status == GPS_STATUS_NONE:
                    g_gps_status = GPS_STATUS_INIT
                    g_longi = str(round(gps.longitude[0], 2))
                    g_lati = str(round(gps.latitude[0], 2))
                    #print("Latitude:", g_longi)
                    #print("Longitude:", g_lati)
                elif g_gps_status == GPS_STATUS_SYNC:
                    g_gps_status = GPS_STATUS_UPDATE
                    g_longi = str(round(gps.longitude[0], 2))
                    g_lati = str(round(gps.latitude[0], 2))
                    #print("Latitude:", g_longi)
                    #print("Longitude:", g_lati)


def connect_to_network():
    # Connect to WiFi
    wlan.active(True)
    # wlan.config(pm = 0xa11140) # Disable power-save mode
    wlan.connect(WIFI_SSID, WIFI_PASSWORD)

    max_wait = 10
    while max_wait > 0:
        if wlan.status() < 0 or wlan.status() >= 3:
            break
        max_wait -= 1
        print("waiting for connection..." + str(wlan.status()))
        time.sleep(1)
    if wlan.status() != 3:
        raise RuntimeError("network connection failed")
    else:
        print("connected")
        status = wlan.ifconfig()
        print("ip = " + status[0])


def ntp_sync_time():
    # Get the current network time and set the system clock
    # print("Local time before synchronization：%s" % str(time.localtime()))
    # ntptime.settime()
    # utctime = ntptime.time() + UTC_OFFSET
    # print("Local time after synchronization：%s" % str(time.localtime(utctime)))
    print("Local time before synchronization：%s" % str(time.localtime()))
    ntptime.host = 'ntp1.aliyun.com'
    ntptime.timeout = 5
    t = ntptime.time() + UTC_OFFSET
    tm = utime.gmtime(t)
    rtc.datetime((tm[0], tm[1], tm[2], tm[6] + 1, tm[3], tm[4], tm[5], 0))
    print("Local time after synchronization：%s" % str(time.localtime()))


def baidu_get_location():
    url = "http://api.map.baidu.com/geocoder?location=" + g_lati + "," + g_longi + "&output=json"
    location = {}
    # print('old url = ' + url)
    # new_url = url.replace("***", g_lati_longi)
    print("request url = " + url)
    res = urequests.get(url=url).json()
    print(res)
    if res["status"] == "OK":
        location['city'] = res["result"]["addressComponent"]["city"]
        location['district'] = res["result"]["addressComponent"]["district"]
        print("location at " + location['city'] + " " + location['district'])
    return location

def qweather_decompress(data):
    FTEXT = 1
    FHCRC = 2
    FEXTRA = 4
    FNAME = 8
    FCOMMENT = 16
    assert data[0] == 0x1f and data[1] == 0x8b
    assert data[2] == 8
    flg = data[3]
    assert flg & 0xe0 == 0
    i = 10
    if flg & FEXTRA:
        i += data[11] << 8 + data[10] + 2
    if flg & FNAME:
        while data[i]:
            i += 1
        i += 1
    if flg & FCOMMENT:
        while data[i]:
            i += 1
        i += 1
    if flg & FHCRC:
        i += 2
    return uzlib.decompress(memoryview(data)[i:], -15)


def qweather_city_search():
    url = "https://geoapi.qweather.com/v2/city/lookup?key=" + QWEATHER_KEY + "&location=" + g_longi + "," + g_lati
    print("request url = " + url)
    res = urequests.get(url=url)
    data = qweather_decompress(res.content).decode()
    dic_res = ujson.loads(data)

    if dic_res['code'] == '200':
        name = dic_res["location"]["name"]
        name_id = dic_res["location"]["id"]
        city = dic_res["location"]["adm2"]
        province = dic_res["location"]["adm1"]
        print('locationa at ' + name_id + ' ' + name + ' ' + city + ' ' + province)


def qweather_weather_now():
    url = "https://devapi.qweather.com/v7/weather/now?key=" + QWEATHER_KEY + "&location=" + g_longi + "," + g_lati
    print("request url = " + url)
    res = urequests.get(url=url)
    data = qweather_decompress(res.content).decode()
    dic_res = ujson.loads(data)
    weather = {}

    if dic_res['code'] == '200':
        weather['temp'] = dic_res["now"]["temp"]
        weather['humi'] = dic_res["now"]["humidity"]
        weather['text'] = dic_res["now"]["text"]
        weather['icon'] = dic_res["now"]["icon"]
        print('weather: temp=' + weather['temp'] + ' hump=' + weather['humi'] + ' text=' + weather['text'])
    return weather

def systick_handle(timer):
    global g_tick_counter
    g_tick_counter += 1
    # print("g_tick_counter = " + str(g_tick_counter))
    if wlan.status() != 3 and wlan.status() != 1:
        wlan.connect(WIFI_SSID, WIFI_PASSWORD)

    if not wlan.isconnected():
        led.toggle()
    else:
        if g_tick_counter % 5 == 0:
            led.toggle()
    display.rect(0, 48, 128, 16, 0, True)
    date = time.localtime()
    nyr = '{}/{}/{}'.format(date[0], date[1], date[2])
    display.text(nyr, 0, 48)
    sfm = '{}/{}/{}'.format(date[3], date[4], date[5])
    display.text(sfm, 0, 56)
    font.text(display, '星期' + WEEK[date[6]], 112-32, 48, show=True)

font.text(display, "WIFI 接入中。。。", 0, 0, show=True)
connect_to_network()
time.sleep_ms(250)
timer.init(freq=5, mode=Timer.PERIODIC, callback=systick_handle)
if wlan.isconnected():
    display.rect(0, 0, 128, 16, 0, True)
    font.text(display, "WIFI 接入成功", 0, 0, show=True)
    ntp_sync_time()
    display.rect(0, 48, 128, 16, 0, True)
    date = time.localtime()
    nyr = '{}/{}/{}'.format(date[0], date[1], date[2])
    display.text(nyr, 0, 48)
    sfm = '{}/{}/{}'.format(date[3], date[4], date[5])
    display.text(sfm, 0, 56)
    font.text(display, '星期' + WEEK[date[6]], 112-32, 48, show=True)
    '''weather = qweather_weather_now()
    if weather:
        display.rect(0, 16, 112, 32, 0, True)
        font.text(display, '温度 ' + weather['temp'], 0, 16, show=False)
        font.text(display, '湿度 ' + weather['humi'], 0, 32, show=False)
        # font.text(display, weather['text'], 0, 48, show=True)
        fb = framebuf.FrameBuffer(weather_ico.ico[weather['icon']], 32, 32, framebuf.MONO_HLSB)
        display.blit(fb, 80, 16)
        display.show()'''
time.sleep_ms(250)
uart_gps.read()
display.rect(0, 0, 128, 16, 0, True)
font.text(display, "定位中。。。", 0, 0, show=True)
time.sleep_ms(250)
sync_start = gps_start = time.ticks_ms()
while True:
    gps_process()

    if wlan.isconnected():
        if time.ticks_diff(time.ticks_ms(), sync_start) > (1000 * 3600):
            sync_start = time.ticks_ms()
            ntp_sync_time()
            weather = qweather_weather_now()
            if weather:
                display.rect(0, 16, 112, 32, 0, True)
                font.text(display, '温度 ' + weather['temp'], 0, 16, show=False)
                font.text(display, '湿度 ' + weather['humi'], 0, 32, show=False)
                # font.text(display, weather['text'], 0, 48, show=True)
                fb = framebuf.FrameBuffer(weather_ico.ico[weather['icon']], 32, 32, framebuf.MONO_HLSB)
                display.blit(fb, 80, 16)
                display.show()

        if time.ticks_diff(time.ticks_ms(), gps_start) > (1000 * 60):
            g_gps_status = GPS_STATUS_SYNC

        if g_gps_status == GPS_STATUS_INIT:
            g_gps_status = GPS_STATUS_IDLE
            display.rect(0, 0, 128, 16, 0, True)
            location = baidu_get_location()
            if location:
                display.rect(0, 0, 128, 16, 0, True)
                #display.text('E' + g_longi, 0, 0)
                #display.text('N' + g_lati, 0, 8)
                display.text(gps.longitude[1] + str(round(gps.longitude[0], 2)), 0, 0)
                display.text(gps.latitude[1] + str(round(gps.latitude[0], 2)), 0, 8)
                font.text(display, location['district'], 63, 0, show=True)
            weather = qweather_weather_now()
            if weather:
                display.rect(0, 16, 112, 32, 0, True)
                font.text(display, '温度 ' + weather['temp'], 0, 16, show=False)
                font.text(display, '湿度 ' + weather['humi'], 0, 32, show=False)
                # font.text(display, weather['text'], 0, 48, show=True)
                fb = framebuf.FrameBuffer(weather_ico.ico[weather['icon']], 32, 32, framebuf.MONO_HLSB)
                display.blit(fb, 80, 16)
                display.show()
        elif g_gps_status == GPS_STATUS_UPDATE:
            g_gps_status = GPS_STATUS_IDLE
            location = baidu_get_location()
            if location:
                display.rect(63, 0, 64, 16, 0, True)
                font.text(display, loca['district'], 63, 0, show=True)
