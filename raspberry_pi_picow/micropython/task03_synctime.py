from machine import Pin, Timer, RTC
import time
import network
import ntptime


WIFI_SSID = "your-ssid"
WIFI_PASSWORD = "your-password"
UTC_OFFSET = 8 * 60 * 60
led = Pin("LED", Pin.OUT)
rtc = RTC()

# Connect to WiFi
wlan = network.WLAN(network.STA_IF)
wlan.active(True)
access_points = wlan.scan()
for AP in access_points:
print(AP)
wlan.connect(WIFI_SSID, WIFI_PASSWORD)
while not wlan.isconnected() and wlan.status() >= 0:
print("Waiting to connect:")
led.toggle()
time.sleep(0.25)
print("Connected to WiFi!")
print(wlan.ifconfig())
# Get the current network time and set the system clock
print("Local time before synchronization：%s" % str(time.localtime()))
ntptime.timeout = 3
ntptime.settime()
utctime = ntptime.time() + UTC_OFFSET
print("Local time after synchronization：%s" % str(time.localtime(utctime)))
