from machine import Pin, I2C, RTC, Timer, UART
import time
import utime
import network
import ntptime
from ssd1306 import SSD1306_I2C
import ufont
import weather_ico
import framebuf


WIFI_SSID = "asr-guest"  # "your-ssid"
WIFI_PASSWORD = "asr123456"  # "your-password"
UTC_OFFSET = 8 * 60 * 60


led = Pin('LED', Pin.OUT)
timer = Timer()
rtc = RTC()
i2c_oled = I2C(0, scl=Pin(9), sda=Pin(8), freq=400000)
display = SSD1306_I2C(128, 64, i2c_oled)
display.clear()
font = ufont.BMFont("unifont-14-12917-16.v3.bmf")
wlan = network.WLAN(network.STA_IF)


def connect_to_network():
    # Connect to WiFi
    wlan.active(True)
    # wlan.config(pm = 0xa11140) # Disable power-save mode
    wlan.connect(WIFI_SSID, WIFI_PASSWORD)

    max_wait = 10
    while max_wait > 0:
        if wlan.status() < 0 or wlan.status() >= 3:
            break
        max_wait -= 1
        print("waiting for connection..." + str(wlan.status()))
        time.sleep(1)
    if wlan.status() != 3:
        raise RuntimeError("network connection failed")
    else:
        print("connected")
        status = wlan.ifconfig()
        print("ip = " + status[0])


def ntp_sync_time():
    # Get the current network time and set the system clock
    '''
    print("Local time before synchronization：%s" % str(time.localtime()))
    ntptime.settime()
    utctime = ntptime.time() + UTC_OFFSET
    print("Local time after synchronization：%s" % str(time.localtime(utctime)))
    '''
    print("Local time before synchronization：%s" % str(time.localtime()))
    ntptime.host = 'ntp1.aliyun.com'
    ntptime.timeout = 5
    t = ntptime.time() + UTC_OFFSET
    tm = utime.gmtime(t)
    rtc.datetime((tm[0], tm[1], tm[2], tm[6] + 1, tm[3], tm[4], tm[5], 0))
    print("Local time after synchronization：%s" % str(time.localtime()))


#connect_to_network()
#if wlan.isconnected():
#    ntp_sync_time()
#buffer = bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x80\x00\x00\x01\x80\x00\x00\x01\x80\x00\x01\x81\x03\x80\x01\xc0\x07\x80\x00\xc7\xe7\x00\x00\x1f\xf0\x00\x00<x\x00\x008\x1c\x00\x00p\x0c\x00\x0ep\x0c`\x0f`\x0e\xf0\x0ep\x0c\xf0\x00p\x0c\x00\x008\x1c\x00\x00<8\x00\x00\x1f\xf0\x00\x00\xc7\xe6\x00\x01\xc0\x07\x00\x01\xc0\x03\x80\x01\x81\x81\x00\x00\x01\x80\x00\x00\x01\x80\x00\x00\x01\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
#font.text(display, "WIFI 接入中。。。", 0, 0, show=True)


buffer = bytearray(b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00|?\x00\x01\x86@\x80\x01\x01\x80\x80\x01\x11\x88\x80\x01\x05\xa0\x80\x00\x83\xc1\x00\x00C\xe3\x00\x00~\xfc\x00\x00L'\x00\x00\x9c\x11\x00\x00\xbf\xfd\x00\x00\xe1\x87\x00\x01\xc1\x83\x80\x02A\x82@\x02A\x82@\x02\xc1\xc2@\x02\xf6>\xc0\x01\xfc=\x80\x01\x18\x18\x80\x01\x88\x10\x80\x00\x8c!\x00\x00\x87\xf1\x00\x00\x7f\xf6\x00\x008\x1c\x00\x00\x0c \x00\x00\x03\xc0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")
fb = framebuf.FrameBuffer(buffer, 32, 32, framebuf.MONO_HLSB)
display.blit(fb, 96, 0)
font.text(display, "Thanks", 0, 0, show=False)
#display.text("eeworld!", 8, 16)
#display.text("Digi-Key!", 16, 32)
font.text(display, "eeworld!", 16, 16, show=False)
font.text(display, "Digi-Key!", 16, 32, show=False)
font.text(display, "Follow me 活动", 0, 48, show=False)
display.show()
