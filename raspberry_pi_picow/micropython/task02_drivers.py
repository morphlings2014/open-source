from machine import I2C, Pin, PWM
from ssd1306 import SSD1306_I2C
import utime
import ufont
import framebuf


i2c = I2C(0, scl=Pin(9), sda=Pin(8), freq=400000)
oled = SSD1306_I2C(128, 64, i2c)
# Raspberry Pi logo as 32x32 bytearray
buffer = bytearray(b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00|?\x00\x01\x86@\x80\x01\x01\x80\x80\x01\x11\x88\x80\x01\x05\xa0\x80\x00\x83\xc1\x00\x00C\xe3\x00\x00~\xfc\x00\x00L'\x00\x00\x9c\x11\x00\x00\xbf\xfd\x00\x00\xe1\x87\x00\x01\xc1\x83\x80\x02A\x82@\x02A\x82@\x02\xc1\xc2@\x02\xf6>\xc0\x01\xfc=\x80\x01\x18\x18\x80\x01\x88\x10\x80\x00\x8c!\x00\x00\x87\xf1\x00\x00\x7f\xf6\x00\x008\x1c\x00\x00\x0c \x00\x00\x03\xc0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")
fb = framebuf.FrameBuffer(buffer, 32, 32, framebuf.MONO_HLSB)
display.blit(fb, 96, 0)
font.text(display, "Thanks", 0, 0, show=False)
#display.text("eeworld!", 8, 16)
#display.text("Digi-Key!", 16, 32)
font.text(display, "eeworld!", 16, 16, show=False)
font.text(display, "Digi-Key!", 16, 32, show=False)
font.text(display, "Follow me �", 0, 48, show=False)
display.show()


tones = {"1": 262, "2": 294, "3": 330, "4": 349, "5": 392, "6": 440, "7": 494, "-": 0}
melody = "1155665-4433221-5544332-5544332-1155665-4433221"
beeper = PWM(Pin(16))
beeper.duty_u16(1000)
for tone in melody:
    freq = tones[tone]
    if freq:
        beeper.freq(freq)
        beeper.duty_u16(1000)
        #beeper = PWM(Pin(16), freq=freq, duty_u16=1000)
    else:
        beeper.duty_u16(0)
        utime.sleep_ms(400)
        beeper.duty_u16(0)
        utime.sleep_ms(100)
beeper.deinit()
