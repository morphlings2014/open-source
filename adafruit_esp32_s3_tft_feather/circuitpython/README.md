```python
>>> help ("modules")
__future__        collections       micropython       terminalio
__main__          countio           msgpack           time
_asyncio          digitalio         neopixel_write    touchio
_bleio            displayio         nvm               traceback
_pixelmap         dualbank          onewireio         ulab
adafruit_bus_device                 errno             os                ulab.numpy
adafruit_bus_device.i2c_device      espidf            ps2io             ulab.numpy.fft
adafruit_bus_device.spi_device      espnow            pulseio           ulab.numpy.linalg
adafruit_pixelbuf espulp            pwmio             ulab.scipy
aesio             fontio            rainbowio         ulab.scipy.linalg
alarm             framebufferio     random            ulab.scipy.optimize
analogbufio       frequencyio       re                ulab.scipy.signal
analogio          gc                rgbmatrix         ulab.scipy.special
array             getpass           rotaryio          ulab.utils
atexit            hashlib           rtc               usb_cdc
audiobusio        i2cperipheral     sdcardio          usb_hid
audiocore         i2ctarget         select            usb_midi
audiomixer        io                sharpdisplay      uselect
binascii          ipaddress         socketpool        vectorio
bitbangio         json              ssl               watchdog
bitmaptools       keypad            storage           wifi
board             math              struct            zlib
builtins          mdns              supervisor
busio             memorymap         synthio
canio             microcontroller   sys

```