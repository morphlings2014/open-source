import time
import board
import digitalio
import displayio
import terminalio
from adafruit_display_text import label
from adafruit_bitmap_font import bitmap_font
import adafruit_imageload


led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

display = board.DISPLAY

# Create a Group
main_group = displayio.Group()

# Create a bitmap with two colors
bitmap = displayio.Bitmap(240, 75, 1)
# Create a two color palette
palette = displayio.Palette(2)
palette[0] = 0x000000
palette[1] = 0xFFFFFF
tile_grid = displayio.TileGrid(bitmap, pixel_shader=palette)

# Set text, font, and color
font = bitmap_font.load_font("/res/opposans_m_12.pcf")

# Create the text label
line1_area = label.Label(
    font, x=0, y=6, text="Follow me 第2期任务", scale=1, color=(0, 255, 255)
)
line2_area = label.Label(
    font, x=0, y=24, text="感谢 eeworld 论坛", scale=1, color=(255, 69, 0)
)
line3_area = label.Label(
    font, x=0, y=42, text="感谢德捷电子", scale=1, color=(235, 30, 30)
)
updating_label = label.Label(
    font, x=0, y=62, text="Time Is : {}".format(time.monotonic()), scale=1, color=(128, 128, 128)
)

main_group.append(tile_grid)
main_group.append(line1_area)
main_group.append(line2_area)
main_group.append(line3_area)
main_group.append(updating_label)


# adafruit logo
adafruit_group = displayio.Group(y=75)
image, palette = adafruit_imageload.load(
    "res/logo_1x.bmp", bitmap=displayio.Bitmap, palette=displayio.Palette
)

tile_grid_adafruit = displayio.TileGrid(image, pixel_shader=palette, tile_width=172, tile_height=60)
adafruit_group.append(tile_grid_adafruit)
main_group.append(adafruit_group)

# Show it
display.show(main_group)

# Loop forever so you can enjoy your image
while True:
    # update text property to change the text showing on the display
    updating_label.text = "Time Is:{}".format(time.monotonic())

    time.sleep(1)
