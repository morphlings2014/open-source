import time
import busio

DFPLAYER_EQ_NORMAL = 0
DFPLAYER_EQ_POP = 1
DFPLAYER_EQ_ROCK = 2
DFPLAYER_EQ_JAZZ = 3
DFPLAYER_EQ_CLASSIC = 4
DFPLAYER_EQ_BASS = 5

DFPLAYER_DEVICE_UDISK = 1
DFPLAYER_DEVICE_SD = 2
DFPLAYER_DEVICE_AUX = 3
DFPLAYER_DEVICE_SLEEP = 4
DFPLAYER_DEVICE_FLASH = 5

DFPLAYER_RECEIVED_LENGTH = 10
DFPLAYER_SEND_LENGTH = 10

TimeOut = 0
WrongStack = 1
DFPlayerCardInserted = 2
DFPlayerCardRemoved = 3
DFPlayerCardOnline = 4
DFPlayerPlayFinished = 5
DFPlayerError = 6
DFPlayerUSBInserted = 7
DFPlayerUSBRemoved = 8
DFPlayerUSBOnline = 9
DFPlayerCardUSBOnline = 10
DFPlayerFeedBack = 11

# STATUS_BUSY = 1
# STATUS_SLEEPING = 2
# STATUS_SERIAL_WRONG_STACK = 3
# STATUS_CHECKSUM_NOT_MATCH = 4
# STATUS_FILE_INDEX_OUT = 5
# STATUS_FILE_MISMATCH = 6
# STATUS_ADVERTISE = 7
Busy = 1
Sleeping = 2
SerialWrongStack = 3
CheckSumNotMatch = 4
FileIndexOut = 5
FileMismatch = 6
Advertise = 7


STACK_HEADER = 0
STACK_VERSION = 1
STACK_LENGTH = 2
STACK_COMMAND = 3
STACK_ACK = 4
STACK_PARAMETER = 5
STACK_CHECKSUM = 7
STACK_END = 9

class dfplayer_mini:
    # functions
    def __init__(self, stream: busio.UART, isACK: bool = True, doReset: bool = True):
        self._serial = stream

        # Private variables
        self._timeOutTimer = 0
        self._timeOutDuration = 500
        self._received = [0x00] * DFPLAYER_RECEIVED_LENGTH
        self._sending = [0x7E, 0xFF, 0x06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0xEF]
        self._receivedIndex = 0
        self.device = DFPLAYER_DEVICE_SD

        # public
        self._handleType: int
        self._handleCommand: int
        self._handleParameter: int
        self._isAvailable: bool = False
        self._isSending: bool = False

    def _sleep_ms(self, ms: int):
        time.sleep(ms/1000)

    def _print_hex(self, bytes):
        l = [hex(int(i)) for i in bytes]
        print(" ".join(l))

    def setTimeOut(self, timeOutDuration: int) -> None:
        self._timeOutDuration = timeOutDuration

    def _arrayToUint16(self, array):
        value = (array[0] << 8) + array[1]
        return value

    def _uint16ToArray(self, value: int, array: list[int], index: int) -> None:
        array[index] = (value >> 8) & 0xFF
        array[index + 1] = value & 0xFF

    def _calculateCheckSum(self, buffer: list[int]) -> int:
        sum = 0
        for i in range(STACK_VERSION, STACK_CHECKSUM):
            sum += buffer[i]
        # revert
        return 0x10000 - sum

    def _enableACK(self):
        self._sending[STACK_ACK] = 0x01

    def _disableACK(self):
        self._sending[STACK_ACK] = 0x00

    def _wait_available(self, duration = None):
        if duration == None:
            duration = self._timeOutDuration
        '''
        # micropython
        timer = time.ticks_ms()
        while not self.available():
            if time.ticks_diff(time.ticks_ms(), timer) > duration:
                return self.handleError(TimeOut)
            time.sleep_ms(1)
        '''
        timer = time.monotonic()
        while not self.available():
            if (time.monotonic() - timer)*1000 > duration:
                return self.handleError(TimeOut)
            time.sleep(0.001)
        return True

    def _send_stack(self, command, argumentHigh: int = 0x00, argumentLow: int = 0x00):
        self._sending[STACK_COMMAND] = command
        self._sending[STACK_PARAMETER] = argumentHigh
        self._sending[STACK_PARAMETER + 1] = argumentLow
        # check sum
        self._uint16ToArray(self._calculateCheckSum(self._sending), self._sending, STACK_CHECKSUM)
        # sum = self._calculateCheckSum(self._sending)
        # self._sending[STACK_CHECKSUM] = (sum >> 8) & 0xFF
        # self._sending[STACK_CHECKSUM + 1] = sum & 0xFF
        # check ack
        if self._sending[STACK_ACK]:
            while self._isSending:
                self._wait_available()

        # Debugging
        # print('send:', end=' ')
        # self._print_hex(self._sending)

        self._serial.write(bytes(self._sending))
        self._timeOutTimer = time.monotonic()
        self._isSending = self._sending[STACK_ACK]

        if not self._sending[STACK_ACK]:
            self._sleep_ms(10)


    def begin(self, stream: busio.UART = None, isACK=True, doReset=True):
        # self._serial = stream

        if isACK:
            self._enableACK()
        else:
            self._disableACK()

        if doReset:
            self.reset()
            self._wait_available(2000)
            self._sleep_ms(200)
        else:
            self._handleType = DFPlayerCardOnline

        return (self.readType() == DFPlayerCardOnline) or (self.readType() == DFPlayerUSBOnline) or not isACK

    def readType(self):
        self._isAvailable = False
        return self._handleType

    def read(self):
        self._isAvailable = False
        return self._handleParameter

    def handleMessage(self, type, parameter: int = 0):
        self._receivedIndex = 0
        self._handleType = type
        self._handleParameter = parameter
        self._isAvailable = True
        return self._isAvailable

    def handleError(self, type, parameter: int = 0):
        self.handleMessage(type, parameter)
        self._is_sending = False
        return False

    def read_command(self):
        self._is_available = False
        return self._handle_command

    def parseStack(self):
        handleCommand = self._received[STACK_COMMAND]
        if handleCommand == 0x41:
            self._isSending = False
            return

        self._handleCommand = handleCommand
        self._handleParameter = self._arrayToUint16(self._received[STACK_PARAMETER:])

        if self._handleCommand == 0x3C or self._handleCommand == 0x3D:
            self.handleMessage(DFPlayerPlayFinished, self._handleParameter)
        elif self._handleCommand == 0x3F:
            if self._handleParameter & 0x01:
                self.handleMessage(DFPlayerUSBOnline, self._handleParameter)
            elif self._handleParameter & 0x02:
                self.handleMessage(DFPlayerCardOnline, self._handleParameter)
            elif self._handleParameter & 0x03:
                self.handleMessage(DFPlayerCardUSBOnline, self._handleParameter)
        elif self._handleCommand == 0x3A:
            if self._handleParameter & 0x01:
                self.handleMessage(DFPlayerUSBInserted, self._handleParameter)
            elif self._handleParameter & 0x02:
                self.handleMessage(DFPlayerCardInserted, self._handleParameter)
        elif self._handleCommand == 0x3B:
            if self._handleParameter & 0x01:
                self.handleMessage(DFPlayerUSBRemoved, self._handleParameter)
            elif self._handleParameter & 0x02:
                self.handleMessage(DFPlayerCardRemoved, self._handleParameter)
        elif self._handleCommand == 0x40:
            self.handleMessage(DFPlayerError, self._handleParameter)
        elif self._handleCommand in [0x3E, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F]:
            self.handleMessage(DFPlayerFeedBack, self._handleParameter)
        else:
            self.handleError(WrongStack)

    def validateStack(self):
        return self._calculateCheckSum(self._received) == self._arrayToUint16(self._received[STACK_CHECKSUM:])

    def available(self):
        byte = self._serial.read(1)
        while byte != None:
            # self._sleep_ms(0)
            if self._receivedIndex == 0:
                self._received[STACK_HEADER] = int.from_bytes(byte, 'little')

                if self._received[STACK_HEADER] == 0x7E:
                    self._receivedIndex += 1
            else:
                self._received[self._receivedIndex] = int.from_bytes(byte, 'little')

                if self._receivedIndex == STACK_VERSION:
                    if self._received[self._receivedIndex] != 0xFF:
                        return self.handleError(WrongStack)
                if self._receivedIndex == STACK_LENGTH:
                    if self._received[self._receivedIndex] != 0x06:
                        return self.handleError(WrongStack)
                elif self._receivedIndex == STACK_END:
                    if self._received[self._receivedIndex] != 0xEF:
                        return self.handleError(WrongStack)
                    else:
                        if self.validateStack():
                            self._receivedIndex = 0
                            self.parseStack()
                            return self._isAvailable
                        else:
                            return self.handleError(WrongStack)
                else:
                    pass
                self._receivedIndex += 1
            byte = self._serial.read(1)
        # print('recv:', end=' ')
        # self._print_hex(self._received)
        return self._isAvailable

    def next(self):
        self._send_stack(0x01)

    def previous(self):
        self._send_stack(0x02)

    def play(self, fileNumber):
        self._send_stack(0x03, fileNumber >> 8, fileNumber & 0xFF)

    def volumeUp(self):
        self._send_stack(0x04)

    def volumeDown(self):
        self._send_stack(0x05)

    def volume(self, volume):
        self._send_stack(0x06, 0, volume)

    def EQ(self, eq):
        self._send_stack(0x07, 0, eq)

    def loop(self, fileNumber):
        self._send_stack(0x08, fileNumber >> 8, fileNumber & 0xFF)

    def outputDevice(self, device):
        self._send_stack(0x09, 0, device)
        self._sleep_ms(200)

    def sleep(self):
        self._send_stack(0x0A)

    def reset(self):
        self._send_stack(0x0C)

    def start(self):
        self._send_stack(0x0D)

    def pause(self):
        self._send_stack(0x0E)

    def playFolder(self, folderNumber, fileNumber):
        self._send_stack(0x0F, folderNumber, fileNumber)

    def outputSetting(self, enable, gain):
        self._send_stack(0x10, enable, gain)

    def enableLoopAll(self):
        self._send_stack(0x11, 0, 0x01)

    def disableLoopAll(self):
        self._send_stack(0x11, 0x00)

    def playMp3Folder(self, fileNumber):
        self._send_stack(0x12, fileNumber >> 8, fileNumber & 0xFF)

    def advertise(self, fileNumber):
        self._send_stack(0x13, fileNumber >> 8, fileNumber & 0xFF)

    def playLargeFolder(self, folderNumber, fileNumber):
        number = folderNumber * 1000 + fileNumber
        self._send_stack(0x14, number >> 8, number & 0xFF)

    def stopAdvertise(self):
        self._send_stack(0x15)

    def stop(self):
        self._send_stack(0x16)

    def loopFolder(self, folderNumber):
        self._send_stack(0x17, folderNumber >> 8, folderNumber & 0xFF)

    def randomAll(self):
        self._send_stack(0x18)

    def enableLoop(self):
        self._send_stack(0x19, 0x00, 0x00)

    def disableLoop(self):
        self._send_stack(0x19, 0x00, 0x01)

    def enableDAC(self):
        self._send_stack(0x1A, 0x00, 0x00)

    def disableDAC(self):
        self._send_stack(0x1A, 0x00, 0x01)

    def readState(self):
        self._send_stack(0x42)
        if self._wait_available():
            if self.readType() == DFPlayerFeedBack:
                return self.read()
            else:
                return -1
        else:
            return -1

    def readVolume(self):
        self._send_stack(0x43)
        if self._wait_available():
            return self.read()
        else:
            return -1

    def read_EQ(self):
        self._send_stack(0x44)
        if self._wait_available():
            if self.read_type() == DFPlayerFeedBack:
                return self.read()
            else:
                return -1
        else:
            return -1

    def read_file_counts(self, device):
        if device == DFPLAYER_DEVICE_UDISK:
            self._send_stack(0x47)
        elif device == DFPLAYER_DEVICE_SD:
            self._send_stack(0x48)
        elif device == DFPLAYER_DEVICE_FLASH:
            self._send_stack(0x49)
        else:
            return -1

        if self._wait_available():
            if self.read_type() == DFPlayerFeedBack:
                return self.read()
            else:
                return -1
        else:
            return -1

    def read_current_file_number(self, device):
        if device == DFPLAYER_DEVICE_UDISK:
            self._send_stack(0x4B)
        elif device == DFPLAYER_DEVICE_SD:
            self._send_stack(0x4C)
        elif device == DFPLAYER_DEVICE_FLASH:
            self._send_stack(0x4D)
        else:
            pass

        if self._wait_available():
            if self.read_type() == DFPlayerFeedBack:
                return self.read()
            else:
                return -1
        else:
            return -1

    def read_file_counts_in_folder(self, folder_number):
        self._send_stack(0x4E, folder_number >> 8, folder_number & 0xFF)
        if self._wait_available():
            if self.read_type() == DFPlayerFeedBack:
                return self.read()
            else:
                return -1
        else:
            return -1

    def read_folder_counts(self):
        self._send_stack(0x4F)
        if self._wait_available():
            if self.read_type() == DFPlayerFeedBack:
                return self.read()
            else:
                return -1
        else:
            return -1

    def read_file_counts(self):
        return self.read_file_counts(DFPLAYER_DEVICE_SD)

    def read_current_file_number(self):
        return self.read_current_file_number(DFPLAYER_DEVICE_SD)
