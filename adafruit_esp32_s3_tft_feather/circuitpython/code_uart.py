import board
import digitalio
import busio
import time


led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT
uart = busio.UART(board.TX, board.RX, baudrate=9600)
uart.reset_input_buffer()
print("start")
uart.write(b'hello world')
while True:
    data = uart.read(1)  # read up to 32 bytes
    # print(data)  # this is a bytearray type

    if data is not None:
        led.value = True

        # convert bytearray to string
        data_string = ''.join([chr(b) for b in data])
        print(data_string, end="")

        led.value = False

# while True:
#     led.value = True
#     time.sleep(0.5)
#     led.value = False
#     time.sleep(0.5)
