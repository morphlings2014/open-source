import time
import board
import busio
import digitalio
import displayio
import rtc
import wifi
import ssl
import socketpool
import adafruit_requests
import zlib
import json
import adafruit_ntp
from adafruit_display_text import label
from adafruit_bitmap_font import bitmap_font
import adafruit_imageload
import weather_ico
import neopixel
# from rainbowio import colorwheel
import random
import dfrobot_dfplayer_mini

SSID = "shanghai2014"
PASSWORD = "zafu0828.samw"
UTC_OFFSET = 8
QWEATHER_KEY = "e20da8dc347d477ca13aabdf522caa0c"
CITYID = '101020100'
WEATHER_ICO_SIZE = 64
# DISPLAY_WIDTH = 240
# DISPLAY_HEIGHT = 125
WEEK = ['一', '二', '三', '四', '五', '六', '日']
BUTTON_CLICK_NO = 0
BUTTON_CLICK_SIGNEL = 1
BUTTON_CLICK_DOUBLE = 2
BUTTON_CLICK_LONG = 3

last_state = False
click_count = 0
last_click_time = 0
last_release_time = 0
button_status = 0
music_status = False


led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT
button = digitalio.DigitalInOut(board.BUTTON)
button.switch_to_input(pull=digitalio.Pull.UP)
pixel = neopixel.NeoPixel(board.NEOPIXEL, 1)
pixel.brightness = 0.3
uart = busio.UART(board.TX, board.RX, baudrate=9600)
display = board.DISPLAY

# Create a Group
main_group = displayio.Group()
# Create a bitmap with two colors
bitmap = displayio.Bitmap(display.width, display.height, 1)
# Create a two color palette
palette = displayio.Palette(2)
palette[0] = 0x000000#0xb5b5b5
palette[1] = 0xFFFFFF
tile_grid = displayio.TileGrid(bitmap, pixel_shader=palette)
# Set text, font, and color
font = bitmap_font.load_font("/res/opposans_m_12.pcf")
# Create the text label
lable_wifi_area = label.Label(
    font, x=60, y=12, text="", scale=1, color=(0, 0, 255)
)
lable_music_area = label.Label(
    font, x=142, y=12, text="", scale=1, color=(0, 0, 255)
)
lable_time_area = label.Label(
    font, x=0, y=118, text="", scale=1, color=(255, 69, 0)
)
main_group.append(tile_grid)
main_group.append(lable_wifi_area)
main_group.append(lable_music_area)
main_group.append(lable_time_area)

# logo
group_logo = displayio.Group(x=0, y=0)
image_logo, palette_logo = adafruit_imageload.load(
    "res/index_mainlogo.bmp", bitmap=displayio.Bitmap, palette=displayio.Palette
)
# tile_grid_logo = displayio.TileGrid(image_logo, pixel_shader=palette_logo, 
#                                     tile_width=172, tile_height=60) # adafruit
tile_grid_logo = displayio.TileGrid(image_logo, pixel_shader=palette_logo, 
                                    tile_width=60, tile_height=30) # eeworld
group_logo.append(tile_grid_logo)
main_group.append(group_logo)

# weather
group_weather= displayio.Group(x=0, y=20)
bitmap_weather = displayio.Bitmap(WEATHER_ICO_SIZE, WEATHER_ICO_SIZE, 2)
tile_grid_weather = displayio.TileGrid(bitmap_weather, pixel_shader=palette, x=160, y=12)
label_weather= label.Label(font, x=6, y=20, text="", scale=1, color=(154, 255, 154))
group_weather.append(label_weather)
group_weather.append(tile_grid_weather)
main_group.append(group_weather)

def ntp_sync_time():
    print("Local time before synchronization:%s" % str(time.localtime()))
    try:
        # ntp = adafruit_ntp.NTP(pool, tz_offset=UTC_OFFSET)
        ntp = adafruit_ntp.NTP(pool, server='ntp1.aliyun.com', tz_offset=UTC_OFFSET)
        rtc.RTC().datetime = ntp.datetime
        print("Local time after synchronization: %s" % str(time.localtime()))
    except OSError as e:
        print("ntp error:{}".format(e))
    # NOTE: This changes the system time so make sure you aren't assuming that time
    # doesn't jump.

def qweather_decompress(data):
    FTEXT = 1
    FHCRC = 2
    FEXTRA = 4
    FNAME = 8
    FCOMMENT = 16
    assert data[0] == 0x1f and data[1] == 0x8b
    assert data[2] == 8
    flg = data[3]
    assert flg & 0xe0 == 0
    i = 10
    if flg & FEXTRA:
        i += data[11] << 8 + data[10] + 2
    if flg & FNAME:
        while data[i]:
            i += 1
        i += 1
    if flg & FCOMMENT:
        while data[i]:
            i += 1
        i += 1
    if flg & FHCRC:
        i += 2
    return zlib.decompress(memoryview(data)[i:], -15)

def qweather_weather_now():
    url = "https://devapi.qweather.com/v7/weather/now?key=" + QWEATHER_KEY + "&location=" + CITYID
    print("request url = " + url)
    response = requests.get(url)
    dict_res = json.loads(qweather_decompress(response.content).decode())
    info = {}

    if dict_res['code'] == '200':
        info =  dict_res["now"]
        print('weather: {}'.format(dict_res["now"]))
    return info

def display_update_time():
    date = time.localtime()
    text_time = "{}/{}/{} {}:{}:{} 星期{}".format(date[0], date[1], date[2], date[3], date[4], date[5], WEEK[date[6]])
    lable_time_area.text = text_time

def display_update_weather(info):
    # text_weather = "温度 {}℃、\n湿度 {}\n天气 {}".format(info['temp'], info['humidity'], info['text'])
    text_weather = "温度:{}℃\t湿度:{}％\n体感:{}℃\t天气 {}\n风向:{} 风力:{}级".format(
        info['temp'], info['humidity'], info['feelsLike'], info['text'], info['windDir'], info['windScale'])
    label_weather.text = text_weather
    for y in range(WEATHER_ICO_SIZE):
        for x in range(WEATHER_ICO_SIZE):
            # get point index in bytearray
            index = x // 8 + y * (WEATHER_ICO_SIZE // 8)
            # git point value
            pixel_value = (weather_ico.ico[info['icon']][index] >> (7 - (x % 8))) & 1
            # pixel_value = (weather_ico.ico['100'][index] >> (x % 8)) & 1
            # set Bitmap point
            bitmap_weather[x, y] = pixel_value

def update_pixel_led(status: bool=True):
    if status:
        # pixel[0] = colorwheel(random.randint(0, 255))
        pixel[0] = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    else:
        pixel[0] = (0, 0, 0)#colorwheel(255)

def dfr_player_mini_setup(player: dfrobot_dfplayer_mini.dfplayer_mini):

    print("DFRobot DFPlayer Mini Demo")
    print("Initializing DFPlayer ... (May take 3~5 seconds)")

    # Use serial to communicate with mp3.
    if player.begin() != True:
        print("Unable to begin:")
        print("    1.Please recheck the connection!")
        print("    2.Please insert the SD card!")
        return False

    print("DFPlayer Mini online.")
    # Set volume value. From 0 to 30
    player.volume(10)
    # Play the first mp3
    # player.play(1)
    return True

def update_music(player, status: bool=True):
    if status:
        player.start()
    else:
        player.stop()

def button_handle():
    global last_state
    global click_count
    global last_click_time
    global last_release_time
    global button_status

    current_state = button.value
    # button pressed
    if (last_state != current_state) and (current_state == False):
        button_status = 1
        last_click_time = time.monotonic()
        print("pressed: {}".format(last_click_time))
    elif current_state == True:
        # pressed before
        if button_status == 1:
            # calculate the time pressed
            last_release_time = time.monotonic()
            press_duration = last_release_time - last_click_time
            if press_duration >= 0.05 and press_duration <= 0.3:
                # waiting for checking double kick
                button_status = 2
                click_count += 1
            elif press_duration > 1.5:
                print("long press")
                button_status = 0
                click_count = 0
                return BUTTON_CLICK_LONG
            # print("released: {}".format(press_duration))
        elif button_status == 2:
            press_duration = time.monotonic() - last_release_time
            if press_duration >= 0.2:
                button_status = 0
                if click_count > 1:
                    click_count = 0
                    print("double press")
                    return BUTTON_CLICK_DOUBLE
                else:
                    click_count = 0
                    print("signal press")
                    return BUTTON_CLICK_SIGNEL

    last_state = current_state
    return BUTTON_CLICK_NO

# Show it
display.show(main_group)

lable_wifi_area.text = "系统初始化"

player = dfrobot_dfplayer_mini.dfplayer_mini(uart)
if dfr_player_mini_setup(player):
    lable_music_area.text = "播放器已连接"
else:
    lable_music_area.text = "播放器未连接"

lable_wifi_area.text = "wifi连接中"
# code handler
while not wifi.radio.ipv4_address:
    try:
        wifi.radio.connect(SSID, PASSWORD)
    except ConnectionError as e:
        print("connect error:{}, retry in 2s".format(e))
    time.sleep(2)

print("get ip:", wifi.radio.ipv4_address)
lable_wifi_area.text = "wifi 已连接"
pool = socketpool.SocketPool(wifi.radio)
requests = adafruit_requests.Session(pool, ssl.create_default_context())
ntp_sync_time()
weather_info = qweather_weather_now()
display_update_time()
display_update_weather(weather_info)


time_start = time.monotonic()
# Loop forever so you can enjoy your image
while True:
    time_diff = time.monotonic() - time_start
    if time_diff > 0.6:
        display_update_time()

    # sync ntp and weather
    if time_diff >= 6 * 60 * 60:
        time_start = time.monotonic()
        ntp_sync_time()
        weather_info = qweather_weather_now()
        display_update_weather(weather_info)

    # button handle
    button_event = button_handle()
    if button_event == BUTTON_CLICK_SIGNEL:
        update_pixel_led()
    elif button_event == BUTTON_CLICK_DOUBLE:
        if music_status:
            music_status = False
        else:
            music_status = True
        update_music(player, music_status)

    elif button_event == BUTTON_CLICK_LONG:
        update_pixel_led(False)
