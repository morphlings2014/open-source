import time
import board
import digitalio
import neopixel
from rainbowio import colorwheel
import random

pixel = neopixel.NeoPixel(board.NEOPIXEL, 1)
pixel.brightness = 0.3

''' example
pixel.fill((255, 0, 0))
time.sleep(0.5)
pixel.fill((0, 255, 0))
time.sleep(0.5)
pixel.fill((0, 0, 255))
time.sleep(0.5)

def rainbow(delay):
    for color_value in range(255):
        pixel[0] = colorwheel(color_value)
        time.sleep(delay)

while True:
    rainbow(0.02)
'''

button = digitalio.DigitalInOut(board.BUTTON)
button.switch_to_input(pull=digitalio.Pull.UP)

while True:
    if not button.value:
        color_value = random.randint(0, 255)
    else:
        pixel[0] = colorwheel(color_value)
        time.sleep(0.02)
