import board
import digitalio
import busio
import time
import dfrobot_dfplayer_mini


led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

def printDetail(type: int, value: int):
    if type == dfrobot_dfplayer_mini.TimeOut:
        print("Time Out!")
    elif type == dfrobot_dfplayer_mini.WrongStack:
        print("Stack Wrong!")
    elif type == dfrobot_dfplayer_mini.DFPlayerCardInserted:
        print("Card Inserted!")
    elif type == dfrobot_dfplayer_mini.DFPlayerCardRemoved:
        print("Card Removed!")
    elif type == dfrobot_dfplayer_mini.DFPlayerCardOnline:
        print("Card Online!")
    elif type == dfrobot_dfplayer_mini.DFPlayerUSBInserted:
        print("USB Inserted!")
    elif type == dfrobot_dfplayer_mini.DFPlayerUSBRemoved:
        print("USB Removed!")
    elif type == dfrobot_dfplayer_mini.DFPlayerPlayFinished:
        print("Number: " + value, end = ' ')
        print("Play Finished!")
    if type == dfrobot_dfplayer_mini.DFPlayerError:
        print("DFPlayerError:", end = ' ')
        if value == dfrobot_dfplayer_mini.Busy:
            print("Card not found")
        elif type == dfrobot_dfplayer_mini.Sleeping:
            print("Sleeping")
        elif type == dfrobot_dfplayer_mini.SerialWrongStack:
            print("Get Wrong Stack")
        elif type == dfrobot_dfplayer_mini.CheckSumNotMatch:
            print("Check Sum Not Match")
        elif type == dfrobot_dfplayer_mini.FileIndexOut:
            print("File Index Out of Bound")
        elif type == dfrobot_dfplayer_mini.FileMismatch:
            print("Cannot Find File")
        elif type == dfrobot_dfplayer_mini.Advertise:
            print("In Advertise")

def dfr_player_mini_setup(player: dfrobot_dfplayer_mini.dfplayer_mini):

    print("DFRobot DFPlayer Mini Demo")
    print("Initializing DFPlayer ... (May take 3~5 seconds)")

    # Use serial to communicate with mp3.
    if player.begin() != True:
        print("Unable to begin:")
        print("    1.Please recheck the connection!")
        print("    2.Please insert the SD card!")
        return False

    print("DFPlayer Mini online.")
    # Set volume value. From 0 to 30
    player.volume(6)
    # Play the first mp3
    player.play(1)
    return True

def dfr_player_mini_loop(player: dfrobot_dfplayer_mini.dfplayer_mini):
    timer = time.monotonic()
    while True:
        if (time.monotonic() - timer > 10):
            timer = time.monotonic()
            # Play next mp3 every 3 second
            # print("play next mp3. {}".time.monotonic())
            player.next()

        if (player.available()):
            # Print the detail message from DFPlayer to handle different errors and states.
            printDetail(player.readType(), player.read())

def dfr_test():
    uart = busio.UART(board.TX, board.RX, baudrate=9600)
    player = dfrobot_dfplayer_mini.dfplayer_mini(uart)
    if dfr_player_mini_setup(player):
        
        dfr_player_mini_loop(player)
        # led.value = True
        # time.sleep(0.5)
        # led.value = False
        # time.sleep(0.5)

dfr_test()
print(time.monotonic())
time.sleep(1)
print(time.monotonic())