"""
CircuitPython exercise run on ESP32-S3,
to display on IPS with ST7789 SPI driver.
dev. board:     ESP32-S3-DevKitC-1-N8R8
CircuitPython:  Adafruit CircuitPython 8.1.0-beta.0 on 2023-03-01

Tested IPS:
- 1.14" 135x240 (RGB) IPS
- 1.54" IPS 240x240 (RGB)
- 2.0" IPS 240(RGB)x320

"""
import os, sys
import board
import busio
import terminalio
import displayio
from adafruit_display_text import label
# import adafruit_st7789
import time

display = board.DISPLAY
# Release any resources currently in use for the displays
displayio.release_displays()

"""
#===init display ======================
disp_blk = board.IO4
disp_cs  = board.IO5
disp_dc  = board.IO6
disp_res = board.IO7
disp_mosi = board.IO15
disp_clk = board.IO16

disp_spi = busio.SPI(clock=disp_clk,
                     MOSI=disp_mosi)

display_bus = displayio.FourWire(
    disp_spi,
    command=disp_dc,
    chip_select=disp_cs,
    reset=disp_res
)
"""
#--- Setup display ---
"""
# for 1.14" 135x240 (RGB) IPS
display = adafruit_st7789.ST7789(display_bus,
                                 width=240,
                                 height=135,
                                 rowstart=40, colstart=53,
                                 rotation=270,
                                 backlight_pin=disp_blk)
"""
"""
# for 1.54" IPS 240x240 (RGB)
display = adafruit_st7789.ST7789(display_bus,
                                 width=240,
                                 height=240,
                                 rowstart=80,
                                 rotation=180,
                                 backlight_pin=disp_blk)
"""
"""
# for 2.0" IPS 240(RGB)x320
display = adafruit_st7789.ST7789(display_bus,
                                 width=240,
                                 height=320,
                                 backlight_pin=disp_blk)
"""
"""
# for 2.0" IPS 240(RGB)x320
# rotate 90 degree
disp_width = 240
disp_height = 320
display = adafruit_st7789.ST7789(display_bus,
                                 width=320,
                                 height=240,
                                 rotation=90,
                                 backlight_pin=disp_blk)
"""
#=======================================
info = os.uname()[4] + "\n" + \
       sys.implementation[0] + " " + os.uname()[3] + "\n" + \
       str(display.width) + "x" + str(display.height)
print("=======================================")
print(info)
print("=======================================")
time.sleep(3)
# Make the display context
bgGroup = displayio.Group()
display.show(bgGroup)

bg_bitmap = displayio.Bitmap(display.width, display.height, 1)  # with one color
bg_palette = displayio.Palette(1)
bg_palette[0] = 0xFFFFFF  # White
bg_sprite = displayio.TileGrid(bg_bitmap, pixel_shader=bg_palette, x=0, y=0)
bgGroup.append(bg_sprite)

colorSet = ((0xFF0000, "RED"),
            (0x00FF00, "GREEN"),
            (0x0000FF, "BLUE"),
            (0xFFFFFF, "WHITE"),
            (0x000000, "BLACK"))

color_bitmap = displayio.Bitmap(display.width-2, display.height-2, 1)  # with one color
color_palette = displayio.Palette(1)
color_palette[0] = 0x000000  # BLACK
color_sprite = displayio.TileGrid(color_bitmap, pixel_shader=color_palette, x=1, y=1)
bgGroup.append(color_sprite)

# Draw label
text_group = displayio.Group(scale=2, x=20, y=120)
text = "Hello World!"
text_area = label.Label(terminalio.FONT, text=text, color=0xFFFFFF)
text_group.append(text_area)  # Subgroup for text scaling
bgGroup.append(text_group)

time.sleep(2)
text_group.scale = 3

while True:
    for i in colorSet:
        time.sleep(2)
        color_palette[0] = i[0]
        text_area.text = i[1]
        text_area.color = i[0] ^ 0xFFFFFF
