'''
import board
import digitalio
import displayio
import time
import terminalio
from adafruit_display_text import label
from adafruit_bitmap_font import bitmap_font

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

display = board.DISPLAY
# Set text, font, and color
text = "Follow me 第2期"
font = bitmap_font.load_font("/res/opposans_m_12.pcf")
color = 0xFF00FF
# Create the text label
text_area = label.Label(font, text=text, color=color)

# Set the location
#text_area.x = 0
#text_area.y = 6
text_area.anchor_point = (0, 0)
text_area.anchored_position = (0, 0)
# Show it
display.show(text_area)

# Setup the file as the bitmap data source
#bitmap = displayio.OnDiskBitmap("/res/logo_1x.bmp")

# Create a TileGrid to hold the bitmap
#tile_grid = displayio.TileGrid(bitmap, pixel_shader=bitmap.pixel_shader)

# Create a Group to hold the TileGrid
#group = displayio.Group()

# Add the TileGrid to the Group
#group.append(tile_grid)

# Add the Group to the Display
#display.show(group)


while True:
    led.value = True
    time.sleep(0.5)
    led.value = False
    time.sleep(0.5)
'''
import board
import displayio
import framebufferio

display = board.DISPLAY

# Create a bitmap with two colors
bitmap = displayio.Bitmap(display.width, display.height, 2)
# Create a two color palette
palette = displayio.Palette(2)
palette[0] = 0x000000
palette[1] = 0xffffff

# Create a TileGrid using the Bitmap and Palette
tile_grid = displayio.TileGrid(bitmap, pixel_shader=palette)

# Create a Group
group = displayio.Group()

# Add the TileGrid to the Group
group.append(tile_grid)

# Add the Group to the Display
display.show(group)

# # Draw a pixel
# bitmap[80, 50] = 1

# # Draw even more pixels
# for x in range(150, 170):
#     for y in range(100, 110):
#         bitmap[x, y] = 1

# Raspberry Pi logo as 32x32 bytearray
image_data = bytearray(b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00|?\x00\x01\x86@\x80\x01\x01\x80\x80\x01\x11\x88\x80\x01\x05\xa0\x80\x00\x83\xc1\x00\x00C\xe3\x00\x00~\xfc\x00\x00L'\x00\x00\x9c\x11\x00\x00\xbf\xfd\x00\x00\xe1\x87\x00\x01\xc1\x83\x80\x02A\x82@\x02A\x82@\x02\xc1\xc2@\x02\xf6>\xc0\x01\xfc=\x80\x01\x18\x18\x80\x01\x88\x10\x80\x00\x8c!\x00\x00\x87\xf1\x00\x00\x7f\xf6\x00\x008\x1c\x00\x00\x0c \x00\x00\x03\xc0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")
for y in range(32):
    for x in range(32):
        # 计算像素在bytearray中的索引
        index = x // 8 + y * (32 // 8)
        # 获取像素的值（0或1）
        pixel_value = (image_data[index] >> (7 - (x % 8))) & 1
        # 设置Bitmap对象的像素值
        bitmap[x, y] = pixel_value
        bitmap[x, y+32] = pixel_value
        bitmap[x, y+64] = pixel_value
        bitmap[x, y+96] = pixel_value

# Loop forever so you can enjoy your image
while True:
    pass
