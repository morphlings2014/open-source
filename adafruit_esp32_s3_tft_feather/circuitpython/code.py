import os, sys
import board
import displayio
import framebufferio
import terminalio
import time
from adafruit_display_text import label

display = board.DISPLAY

# # Create a bitmap with two colors
# bitmap = displayio.Bitmap(display.width, display.height, 2)
# # Create a two color palette
# palette = displayio.Palette(2)
# palette[0] = 0x000000
# palette[1] = 0xffffff

# # Create a TileGrid using the Bitmap and Palette
# tile_grid = displayio.TileGrid(bitmap, pixel_shader=palette)

# # Create a Group
# group = displayio.Group()

# # Add the TileGrid to the Group
# group.append(tile_grid)

# # Add the Group to the Display
# display.show(group)

# # # Draw a pixel
# # bitmap[80, 50] = 1

# # # Draw even more pixels
# # for x in range(150, 170):
# #     for y in range(100, 110):
# #         bitmap[x, y] = 1

# # Raspberry Pi logo as 32x32 bytearray
# image_data = bytearray(b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00|?\x00\x01\x86@\x80\x01\x01\x80\x80\x01\x11\x88\x80\x01\x05\xa0\x80\x00\x83\xc1\x00\x00C\xe3\x00\x00~\xfc\x00\x00L'\x00\x00\x9c\x11\x00\x00\xbf\xfd\x00\x00\xe1\x87\x00\x01\xc1\x83\x80\x02A\x82@\x02A\x82@\x02\xc1\xc2@\x02\xf6>\xc0\x01\xfc=\x80\x01\x18\x18\x80\x01\x88\x10\x80\x00\x8c!\x00\x00\x87\xf1\x00\x00\x7f\xf6\x00\x008\x1c\x00\x00\x0c \x00\x00\x03\xc0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")
# for y in range(32):
#     for x in range(32):
#         # 计算像素在bytearray中的索引
#         index = x // 8 + y * (32 // 8)
#         # 获取像素的值（0或1）
#         pixel_value = (image_data[index] >> (7 - (x % 8))) & 1
#         # 设置Bitmap对象的像素值
#         bitmap[x, y] = pixel_value
#         bitmap[x, y+32] = pixel_value
#         bitmap[x, y+64] = pixel_value
#         bitmap[x, y+96] = pixel_value

# # Loop forever so you can enjoy your image
# while True:
#     pass

info = os.uname()[4] + "\n" + \
       sys.implementation[0] + " " + os.uname()[3] + "\n" + \
       str(display.width) + "x" + str(display.height)
print("=======================================")
print(info)
print("=======================================")
time.sleep(3)
# Make the display context
bgGroup = displayio.Group()
display.show(bgGroup)

bg_bitmap = displayio.Bitmap(display.width, display.height, 1)  # with one color
bg_palette = displayio.Palette(1)
bg_palette[0] = 0xFFFFFF  # White
bg_sprite = displayio.TileGrid(bg_bitmap, pixel_shader=bg_palette, x=0, y=0)
bgGroup.append(bg_sprite)

colorSet = ((0xFF0000, "RED"),
            (0x00FF00, "GREEN"),
            (0x0000FF, "BLUE"),
            (0xFFFFFF, "WHITE"),
            (0x000000, "BLACK"))

color_bitmap = displayio.Bitmap(display.width-2, display.height-2, 1)  # with one color
color_palette = displayio.Palette(1)
color_palette[0] = 0x000000  # BLACK
color_sprite = displayio.TileGrid(color_bitmap, pixel_shader=color_palette, x=1, y=1)
bgGroup.append(color_sprite)

# Draw label
text_group = displayio.Group(scale=2, x=20, y=120)
text = "Hello World!"
text_area = label.Label(terminalio.FONT, text=text, color=0xFFFFFF)
text_group.append(text_area)  # Subgroup for text scaling
bgGroup.append(text_group)

time.sleep(2)
text_group.scale = 3

while True:
    for i in colorSet:
        time.sleep(2)
        color_palette[0] = i[0]
        text_area.text = i[1]
        text_area.color = i[0] ^ 0xFFFFFF
