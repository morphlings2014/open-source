import time
import board
import neopixel
from rainbowio import colorwheel
import rtc
import wifi
import ssl
import socketpool
import adafruit_requests
import zlib
import json
import adafruit_ntp

# 设置要连接的wifi和密码
SSID = "shanghai2014"
PASSWORD = "zafu0828.samw"
UTC_OFFSET = 8
QWEATHER_KEY = "e20da8dc347d477ca13aabdf522caa0c"
CITYID = '101020100'

pixel = neopixel.NeoPixel(board.NEOPIXEL, 1)
pixel.brightness = 0.3


def rainbow(delay):
    for color_value in range(255):
        pixel[0] = colorwheel(color_value)
        time.sleep(delay)

def qweather_decompress(data):
    FTEXT = 1
    FHCRC = 2
    FEXTRA = 4
    FNAME = 8
    FCOMMENT = 16
    assert data[0] == 0x1f and data[1] == 0x8b
    assert data[2] == 8
    flg = data[3]
    assert flg & 0xe0 == 0
    i = 10
    if flg & FEXTRA:
        i += data[11] << 8 + data[10] + 2
    if flg & FNAME:
        while data[i]:
            i += 1
        i += 1
    if flg & FCOMMENT:
        while data[i]:
            i += 1
        i += 1
    if flg & FHCRC:
        i += 2
    return zlib.decompress(memoryview(data)[i:], -15)

def ntp_sync_time():
    print("Local time before synchronization：%s" % str(time.localtime()))
    ntp = adafruit_ntp.NTP(pool, tz_offset=UTC_OFFSET)
    #ntp = adafruit_ntp.NTP(pool, server='ntp1.aliyun.com', tz_offset=0)
    # NOTE: This changes the system time so make sure you aren't assuming that time
    # doesn't jump.
    rtc.RTC().datetime = ntp.datetime
    print("Local time after synchronization：%s" % str(time.localtime()))

def qweather_weather_now():
    url = "https://devapi.qweather.com/v7/weather/now?key=" + QWEATHER_KEY + "&location=" + CITYID
    print("request url = " + url)
    response = requests.get(url)
    dic_res = json.loads(qweather_decompress(response.content).decode())
    weather = {}

    if dic_res['code'] == '200':
        weather['temp'] = dic_res["now"]["temp"]
        weather['humi'] = dic_res["now"]["humidity"]
        weather['text'] = dic_res["now"]["text"]
        weather['icon'] = dic_res["now"]["icon"]
        print('weather: temp=' + weather['temp'] + ' hump=' + weather['humi'] + ' text=' + weather['text'])
    return weather

# code handler
while not wifi.radio.ipv4_address:
    try:
        wifi.radio.connect(SSID, PASSWORD)
    except ConnectionError as e:
        print("connecti error:{},retry in 2s".format(e))
    time.sleep(2)

print("get ip:", wifi.radio.ipv4_address)
pool = socketpool.SocketPool(wifi.radio)
requests = adafruit_requests.Session(pool, ssl.create_default_context())
ntp_sync_time()
#qweather_weather_now()

while True:
    rainbow(0.02)
