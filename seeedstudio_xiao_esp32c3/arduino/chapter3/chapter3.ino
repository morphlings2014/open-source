/*
 Play a melody
*/

#include "pitches.h"

#define BUZZER_PIN    D2

// 记录曲子的音符
int melody[] = { 0, 0, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B4, NOTE_D5, NOTE_C5, 
                 NOTE_A4, 0, NOTE_C4, NOTE_E4, NOTE_A4, NOTE_B4, 0, NOTE_E4, NOTE_G4, NOTE_B4, 
                 NOTE_C5, 0, NOTE_E4, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B4, NOTE_D5, NOTE_C5,
                 NOTE_A4, 0, NOTE_C4, NOTE_E4, NOTE_A4, NOTE_B4, 0, NOTE_E4, NOTE_C5, NOTE_B4,
                 NOTE_A4
               };

// 音符持续时间：4为四分音符，8为八分音符
int noteDurations[] = { 4, 4, 8, 8, 8, 8, 8, 8, 8, 8, 
                        4, 8, 8, 8, 8, 4, 8, 8, 8, 8, 
                        4, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
                        4, 8, 8, 8, 8, 4, 8, 8, 8, 8,
                        4, 4, 8, 8, 8, 8, 8, 8, 8, 8, 
                        4
                      };

void setup() {

  // iterate over the notes of the melody:
  for(int thisNote = 0; thisNote < sizeof(melody)/sizeof(melody[0]); thisNote++) {

    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000/noteDurations[thisNote];
    tone(BUZZER_PIN, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);

    // stop the tone playing:
    noTone(BUZZER_PIN);
  }
  
}

void loop() {
  
}
