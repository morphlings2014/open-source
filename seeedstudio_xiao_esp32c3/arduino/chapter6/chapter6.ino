// Font file is stored on SD card
#include "FS.h"
#include "SD.h"

// Graphics and font library
#include <TFT_eSPI.h>
#include <SPI.h>
#include "I2C_BM8563.h"
#include <Adafruit_AHTX0.h>
#include <WiFi.h>

#define TFT_GREY 0x5AEB
#define MAX_IMAGE_WIDTH 240 // Adjust for your images
#define TIME_ZONE   8 * 3600
#define SHOW_PERIOD_TIME    1000
#define SAVE_PERIOD_TIME    5000
#define NTP_SYNC_TIME       1000 * 60 * 60 

Adafruit_AHTX0 aht;
TFT_eSPI tft = TFT_eSPI();  // Invoke library
I2C_BM8563 rtc(I2C_BM8563_DEFAULT_ADDRESS, Wire);

const char *ssid     = "shanghai2014";
const char *password = "zafu0828.samw"; // Set before Complie
const char* ntpServer = "time.cloudflare.com"; // Set before Complie
I2C_BM8563_DateTypeDef dateStruct;
I2C_BM8563_TimeTypeDef timeStruct;
sensors_event_t humidity, temp;
int illuminance = 0;
int xPos = 0;
int yPos = 0;
unsigned long previousShowMillis = 0;  //will store last time
unsigned long previousSaveMillis = 0;  //will store last time
unsigned long previousSyncMillis = 0;  //will store last time

// -------------------------------------------------------------------------
// Setup
// -------------------------------------------------------------------------
void setup(void) {
  Serial.begin(115200); // Used for messages

  // Display initialization
  tft.init();
  pinMode(D2, OUTPUT);
  // Initialise the SD library before the TFT so the chip select is defined
  if (!SD.begin(D2)) {
    Serial.println("Card Mount Failed 65");
    return;
  }
  uint8_t cardType = SD.cardType();

  if (cardType == CARD_NONE) {
    Serial.println("No SD card attached");
    return;
  }

  Serial.print("SD Card Type: ");
  if (cardType == CARD_MMC) {
    Serial.println("MMC");
  } else if (cardType == CARD_SD) {
    Serial.println("SDSC");
  } else if (cardType == CARD_SDHC) {
    Serial.println("SDHC");
  } else {
    Serial.println("UNKNOWN");
  }

  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
  Serial.printf("SD Card Size: %lluMB\n", cardSize);

  // Initialise the TFT after the SD card!
  tft.init();
  tft.setRotation(0);
  tft.fillScreen(TFT_BLACK);

  // listDir(SD, "/", 0);

  Serial.println("SD and TFT initialisation done.");

  if (!aht.begin()) {
    Serial.println("Could not find AHT? Check wiring");
    while (1) delay(10);
  } else {
    Serial.println("AHT10 or AHT20 found");
  }
  
  // Connect to an access point
  WiFi.begin(ssid, password);
  Serial.print("Connecting to Wi-Fi ");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" CONNECTED");

  // Set ntp time to local
  configTime(TIME_ZONE, 0, ntpServer);

  // Init I2C
  Wire.begin();

  // Init RTC
  rtc.begin();

  Serial.println("Initialisation done.");

  // Wrap test at right and bottom of screen
  // tft.setTextWrap(true, true);
  // Name of font file (library adds leading / and .vlw)
  String fileName = "Final-Frontier-28";
  // Font and background colour, background colour is used for anti-alias blending
  // tft.setTextColor(TFT_BLUE, TFT_BLACK);
  // Load the font
  tft.loadFont(fileName, SD);       // Use font stored on SD
  // Display all characters of the font
  // tft.showFont(2000);

  // If the data.txt file doesn't exist
  // Create a file on the SD card and write the data labels
  File file = SD.open("/data.txt");
  if(!file) {
    Serial.println("File doens't exist");
    Serial.println("Creating file...");
    writeFile(SD, "/data.txt", "Reading Time, Temperature, Humidity, Illuminance\r\n");
  }
  else {
    Serial.println("File already exists");  
  }
  file.close();
}

// -------------------------------------------------------------------------
// Main loop
// -------------------------------------------------------------------------
void loop() {
  unsigned long currentMillis = millis(); // store the current time
  if ((currentMillis - previousShowMillis) > SHOW_PERIOD_TIME) { // check if 1000ms passed
    previousShowMillis = currentMillis;   // save the last time
    updateTime();
    updateSensor();
    updateShow();
  }
  if ((currentMillis - previousSyncMillis) > SHOW_PERIOD_TIME) {
    previousSyncMillis = currentMillis;   // save the last time
    syncNTP();
  }

  if ((currentMillis - previousSaveMillis) > SAVE_PERIOD_TIME) {
    previousSaveMillis = currentMillis;   // save the last time
    logSDCard();
  }
  delay(200);
}

void syncNTP(void) {
  // Get local time
  struct tm timeInfo;
  if (getLocalTime(&timeInfo)) {
    // Set RTC time
    I2C_BM8563_TimeTypeDef timeStruct;
    timeStruct.hours   = timeInfo.tm_hour;
    timeStruct.minutes = timeInfo.tm_min;
    timeStruct.seconds = timeInfo.tm_sec;
    rtc.setTime(&timeStruct);

    // Set RTC Date
    I2C_BM8563_DateTypeDef dateStruct;
    dateStruct.weekDay = timeInfo.tm_wday;
    dateStruct.month   = timeInfo.tm_mon + 1;
    dateStruct.date    = timeInfo.tm_mday;
    dateStruct.year    = timeInfo.tm_year + 1900;
    rtc.setDate(&dateStruct);
  }
}

void updateTime(void) {
  // Get RTC
  rtc.getDate(&dateStruct);
  rtc.getTime(&timeStruct);

  // Print RTC
  Serial.printf("%04d/%02d/%02d %02d:%02d:%02d\n",
                dateStruct.year,
                dateStruct.month,
                dateStruct.date,
                timeStruct.hours,
                timeStruct.minutes,
                timeStruct.seconds
               );
}

void updateSensor(void) {
  aht.getEvent(&humidity, &temp);// populate temp and humidity objects with fresh data
  Serial.print("Temperature: "); Serial.print(temp.temperature); Serial.println(" degrees C");
  Serial.print("Humidity: "); Serial.print(humidity.relative_humidity); Serial.println("% rH");

  illuminance = analogRead(A0);
  illuminance = map(illuminance, 0, 800, 0, 100);
  Serial.print("Illuminance: "); Serial.print(illuminance); Serial.println(" lux");

  // xPos = analogRead(A1);
  // yPos = analogRead(A2);
}

void updateShow() {
  // delay(200);
  // tft.fillScreen(TFT_WHITE);
  tft.fillRect(16, 60, 224, 84, TFT_BLACK);
  tft.fillRect(50, 144, 140, 56, TFT_BLACK);
  tft.setTextColor(TFT_BLUE, TFT_BLACK);
  tft.drawString("temp:", 16, 60); tft.drawFloat(temp.temperature, 2, 112, 60);
  tft.drawString("humi:", 16, 88); tft.drawFloat(humidity.relative_humidity, 2, 112, 88);
  tft.setTextColor(TFT_GREEN, TFT_BLACK);
  tft.drawString("illu:", 16, 116); tft.drawNumber(illuminance, 112, 116);
  tft.setTextColor(TFT_WHITE, TFT_BLACK);
  char dataMessage[128];
  sprintf(dataMessage, "%04d/%02d/%02d", dateStruct.year, dateStruct.month, dateStruct.date);
  tft.drawString(dataMessage, 50, 144);
  sprintf(dataMessage, "%02d:%02d:%02d", timeStruct.hours, timeStruct.minutes, timeStruct.seconds);
  tft.drawString(dataMessage, 50, 172);
}


// Write the sensor readings on the SD card
void logSDCard() {
  char dataMessage[256];
  sprintf(dataMessage, "%04d/%02d/%02d %02d:%02d:%02d temp:%.2f humi:%.2f lux:%d\r\n",
                dateStruct.year, dateStruct.month, dateStruct.date,
                timeStruct.hours, timeStruct.minutes, timeStruct.seconds,
                temp.temperature, humidity.relative_humidity, illuminance);
  Serial.print("Save data: ");
  Serial.print(dataMessage);
  appendFile(SD, "/data.txt", dataMessage);
}

// Write to the SD card (DON'T MODIFY THIS FUNCTION)
void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if(!file) {
    Serial.println("Failed to open file for writing");
    return;
  }
  if(file.print(message)) {
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

// Append data to the SD card (DON'T MODIFY THIS FUNCTION)
void appendFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if(!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if(file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}
