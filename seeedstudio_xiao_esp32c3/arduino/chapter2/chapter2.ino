/*
 An example analogue clock using a TFT LCD screen to show the time
 use of some of the drawing commands with the library.

 For a more accurate clock, it would be better to use the RTClib library.
 But this is just a demo. 
 
 This sketch uses font 4 only.

 Make sure all the display driver and pin connections are correct by
 editing the User_Setup.h file in the TFT_eSPI library folder.

 #########################################################################
 ###### DON'T FORGET TO UPDATE THE User_Setup.h FILE IN THE LIBRARY ######
 #########################################################################
 
 Based on a sketch by Gilchrist 6/2/2014 1.0
 */

#include <SPI.h>
#include <TFT_eSPI.h> // Hardware-specific library
#include <PNGdec.h>
#include "panda.h" // Image is stored here in an 8 bit array

#define TFT_GREY 0x5AEB
#define MAX_IMAGE_WIDTH 240 // Adjust for your images

TFT_eSPI tft = TFT_eSPI();       // Invoke custom library
PNG png; // PNG decoder inatance

int16_t xpos = 0;
int16_t ypos = 0;

void setup(void) {
  
  Serial.begin(115200);

  // Initialise the TFT
  tft.init();
  tft.setRotation(0);
  // tft.fillScreen(TFT_BLACK);
  // tft.fillScreen(TFT_RED);
  // tft.fillScreen(TFT_GREEN);
  // tft.fillScreen(TFT_BLUE);
  tft.fillScreen(TFT_BLACK);
  // tft.fillScreen(TFT_GREY);
  // tft.setTextColor(TFT_WHITE, TFT_GREY);  // Adding a background colour erases previous text automatically
  tft.setTextColor(TFT_RED);
  
  Serial.println("\r\nInitialisation done.");
}

void loop() {
  int16_t rc = png.openFLASH((uint8_t *)panda, sizeof(panda), pngDraw);
  if (rc == PNG_SUCCESS) {
    Serial.println("Successfully opened png file");
    Serial.printf("image specs: (%d x %d), %d bpp, pixel type: %d\n", png.getWidth(), png.getHeight(), png.getBpp(), png.getPixelType());
    tft.startWrite();
    uint32_t dt = millis();
    rc = png.decode(NULL, 0);
    Serial.print(millis() - dt); Serial.println("ms");
    tft.endWrite();
    // png.close(); // not needed for memory->memory decode
  }

  delay(1000);
  tft.fillScreen(random(0x10000));
}

//=========================================v==========================================
//                                      pngDraw
//====================================================================================
// This next function will be called during decoding of the png file to
// render each image line to the TFT.  If you use a different TFT library
// you will need to adapt this function to suit.
// Callback function to draw pixels to the display
void pngDraw(PNGDRAW *pDraw) {
  uint16_t lineBuffer[MAX_IMAGE_WIDTH];
  png.getLineAsRGB565(pDraw, lineBuffer, PNG_RGB565_BIG_ENDIAN, 0xffffffff);
  tft.pushImage(xpos, ypos + pDraw->y, pDraw->iWidth, 1, lineBuffer);
}
