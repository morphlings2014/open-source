/*
 An example analogue clock using a TFT LCD screen to show the time
 use of some of the drawing commands with the library.

 For a more accurate clock, it would be better to use the RTClib library.
 But this is just a demo. 
 
 This sketch uses font 4 only.

 Make sure all the display driver and pin connections are correct by
 editing the User_Setup.h file in the TFT_eSPI library folder.

 #########################################################################
 ###### DON'T FORGET TO UPDATE THE User_Setup.h FILE IN THE LIBRARY ######
 #########################################################################
 
 Based on a sketch by Gilchrist 6/2/2014 1.0
 */

#include <SPI.h>
#include <TFT_eSPI.h> // Hardware-specific library
#include "I2C_BM8563.h"
#include <Adafruit_AHTX0.h>

Adafruit_AHTX0 aht;

#define TFT_GREY 0x5AEB
#define MAX_IMAGE_WIDTH 240 // Adjust for your images
TFT_eSPI tft = TFT_eSPI();       // Invoke custom library

unsigned long previousMillis = 0;  //will store last time LED was blinked
const long period = 1000;         // period at which to blink in ms
sensors_event_t humidity, temp;

void setup(void) {
  
  Serial.begin(115200);

  // Initialise the TFT
  tft.init();
  tft.setRotation(0);
  tft.fillScreen(TFT_BLACK);
  // tft.fillScreen(TFT_GREY);
  tft.setTextColor(TFT_WHITE, TFT_GREY);  // Adding a background colour erases previous text automatically
  // tft.setTextColor(TFT_RED);

  // Initialise the aht
  if (!aht.begin()) {
    Serial.println("Could not find AHT? Check wiring");
    while (1) delay(10);
  } else {
    Serial.println("AHT10 or AHT20 found");
  }
  
  Serial.println("\r\nInitialisation done.");
}

void loop() {
  unsigned long currentMillis = millis(); // store the current time
  if (currentMillis - previousMillis >= period) { // check if 1000ms passed
    previousMillis = currentMillis;   // save the last time you blinked the LED
    aht.getEvent(&humidity, &temp);// populate temp and humidity objects with fresh data
  }

  char buffer[64];
  sprintf(buffer, "Temperature: %.2f degrees C", temp.temperature);
  Serial.println(buffer);
  tft.drawString(buffer, 16, 64);
  sprintf(buffer, "Humidity: %.2f% rH", humidity.relative_humidity);
  Serial.println(buffer);
  tft.drawString(buffer, 16, 76);

  int value = analogRead(A0);
  value = map(value, 0, 800, 0, 100);
  sprintf(buffer, "Light Sensor: %d adv", value);
  Serial.println(buffer);
  tft.drawString(buffer, 16, 88);

  int xValue = analogRead(A1);
  int yValue = analogRead(A2);
  sprintf(buffer, "The X and Y coordinate is: %d,%d", xValue, yValue);
  Serial.println(buffer);
  tft.drawString(buffer, 16, 100);

  delay(200);
  tft.fillScreen(random(0x10000));
}
