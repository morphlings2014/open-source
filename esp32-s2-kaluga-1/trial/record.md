# 记录

# chapter1
ESP32S2的开发方式和ESP32是一样的，支持 ESP-IDF、Arduino、PlatfromIO 等多种开发方式，大家可以依据自己的习惯去选择。

这次申请板卡就是为了学习 ESP-IDF 的，所以准备用这种方式开发。espressif 提供了详细的[ 文档 ](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32s2/index.html)支持，里面介绍了搭建环境，快速上手以及 API 的介绍。

## 环境搭建
关于开发环境的搭建，文档中介绍了 3 种 OS 环境下的命令行方式开发。除此之外，其实还有VSCODE，ECLISPE 等安装插件的可选方式开发。这里我选择是 VSCODE 安装插件的插件，因为其他几种方式都遇到了问题，当然 VSCode 在安装的过程种也遇到了各种问题，但是好在最终都解决了（艰难.jpg）
1. 安装 Java、python、git 等必要组件
1. 打开 vscode extensions 目录，搜索 espressif idf，点击安装。可以看到 espressif idf 插件的 detail 描述还是还丰富的，介绍了必要的环境，如何使用等
1. 下载 esp-idf 仓库，由于包含了很多COMPONENT，文件还是挺大的，但是 github 的下载速度你懂的，所以上网还是要`科学`
1. 新建一个工作空间，vscode 方式打开，按 F1 （笔记本可能需要+ Fn 按键），往下找到 ESP-IDF:Configure ESP-IDF extension
1. 点击开始，选择本地的 python 路径
1. 选择之前下载到本地的 esp-idf 版本，当然也可以直接版本，让 VSCODE 自行下载，但是速度同样非常慢。校验之后下一步
1. 之前没有安装过 ESP-IDF Tools 的，需要选择 Download 选项，选择下载地址之后会自动下载安装，耐心等待（下载速度是真的慢，还容易失败，我装了好几次，真的要崩溃）；之前安装过的可以选择 skip 跳过，跳入相应的地址即可
1. 等待安装成功之后，可以通过 F1 目录查找 ESP:Show ESP-IDF Examples Projects，选择之后可以看到有很多的示例程序，但是像蓝牙、外设的部分例程跑 ESPS2 的芯片会有问题，兼容性还是有些问题，后续应该会慢慢完善。
1. 这边简单选择一个 hello_world 例程测试下环境是否搭建成功。
1. 装完插件之后，在 VSCODE 的底部可以看到几个快捷键，配置、编译

# chapter2
之前安装环境已经搭建完成，现在就可以进行应用开发了。DEMO 的例程在 ESP32 上面应该都是可以运行的，但是 ESP32S2 上面存在兼容性的问题，有一些外设和蓝牙的例程会运行失败，我这边也没有全部测试。

## 控制台例程
类似于 linux 的控制台，可以通过输入指令的方式运行你期望的函数，前提功能实现需要提前预制，是非常实用的一个组件，只需要一个串口。

### 工程源码

### 控制台的初始化

```c
static void initialize_console(void)
{
    /* Drain stdout before reconfiguring it */
    fflush(stdout);
    fsync(fileno(stdout));

    /* Disable buffering on stdin */
    setvbuf(stdin, NULL, _IONBF, 0);

    /* Minicom, screen, idf_monitor send CR when ENTER key is pressed */
    esp_vfs_dev_uart_port_set_rx_line_endings(CONFIG_ESP_CONSOLE_UART_NUM, ESP_LINE_ENDINGS_CR);
    /* Move the caret to the beginning of the next line on '\n' */
    esp_vfs_dev_uart_port_set_tx_line_endings(CONFIG_ESP_CONSOLE_UART_NUM, ESP_LINE_ENDINGS_CRLF);

    /* Configure UART. Note that REF_TICK is used so that the baud rate remains
     * correct while APB frequency is changing in light sleep mode.
     */
    const uart_config_t uart_config = {
            .baud_rate = CONFIG_ESP_CONSOLE_UART_BAUDRATE,
            .data_bits = UART_DATA_8_BITS,
            .parity = UART_PARITY_DISABLE,
            .stop_bits = UART_STOP_BITS_1,
            .source_clk = UART_SCLK_REF_TICK,
    };
    /* Install UART driver for interrupt-driven reads and writes */
    ESP_ERROR_CHECK( uart_driver_install(CONFIG_ESP_CONSOLE_UART_NUM,
            256, 0, 0, NULL, 0) );
    ESP_ERROR_CHECK( uart_param_config(CONFIG_ESP_CONSOLE_UART_NUM, &uart_config) );

    /* Tell VFS to use UART driver */
    esp_vfs_dev_uart_use_driver(CONFIG_ESP_CONSOLE_UART_NUM);

    /* Initialize the console */
    esp_console_config_t console_config = {
            .max_cmdline_args = 8,
            .max_cmdline_length = 256,
#if CONFIG_LOG_COLORS
            .hint_color = atoi(LOG_COLOR_CYAN)
#endif
    };
    ESP_ERROR_CHECK( esp_console_init(&console_config) );

    /* Configure linenoise line completion library */
    /* Enable multiline editing. If not set, long commands will scroll within
     * single line.
     */
    linenoiseSetMultiLine(1);

    /* Tell linenoise where to get command completions and hints */
    linenoiseSetCompletionCallback(&esp_console_get_completion);
    linenoiseSetHintsCallback((linenoiseHintsCallback*) &esp_console_get_hint);

    /* Set command history size */
    linenoiseHistorySetMaxLen(100);

    /* Don't return empty lines */
    linenoiseAllowEmpty(false);

#if CONFIG_STORE_HISTORY
    /* Load command history from filesystem */
    linenoiseHistoryLoad(HISTORY_PATH);
#endif
}
```

### 控制台函数注册
通过注册自己需要的命令，esp_console_cmd_register()，函数传参是一个 esp_console_cmd_t 的结构体
```c
typedef struct {
    /**
     * Command name. Must not be NULL, must not contain spaces.
     * The pointer must be valid until the call to esp_console_deinit.
     */
    const char *command;
    /**
     * Help text for the command, shown by help command.
     * If set, the pointer must be valid until the call to esp_console_deinit.
     * If not set, the command will not be listed in 'help' output.
     */
    const char *help;
    /**
     * Hint text, usually lists possible arguments.
     * If set to NULL, and 'argtable' field is non-NULL, hint will be generated
     * automatically
     */
    const char *hint;
    /**
     * Pointer to a function which implements the command.
     */
    esp_console_cmd_func_t func;
    /**
     * Array or structure of pointers to arg_xxx structures, may be NULL.
     * Used to generate hint text if 'hint' is set to NULL.
     * Array/structure which this field points to must end with an arg_end.
     * Only used for the duration of esp_console_cmd_register call.
     */
    void *argtable;
} esp_console_cmd_t;
```

### 运行演示
可以看到输入提前注册的指令之后就能运行想对应的函数

## 结尾
控制台可以脱离仿真器，辅助用户进行代码调试和问题检测，是一个非常不错的组件

# chapter3
通过 https 方式获取天气信息，天气获取源我选择的和风天气的接口，采用的 https 的方式，介于刚开始使用 IDF, 对于网络和加密信息配置等还不是很熟悉，可以直接打开 httpss_request 例程，基本需要的配置就不要自己配，出现问题时直接调试即可，非常方便

1. 例程是通过固定 WIFI 的方式连接网络，所以需要先修改 WIFI 账户和密码，点击 VSCODE 底部的 GUI 配置，找到例程配置，修改账户密码

1. 修改例程的 web_server 和 web_port，和请求 url，改成和风天气提供的即可

1. 修改 request 请求，和连接的服务器地址

1. 编译运行，看到如下结果

1. 以上就是 https 的使用

# chapter4
通过 mqtt 方式连接 onenet 云，没有在例程里面看到 mqtt 的例程，有点不应该啊，但是在 IDF SDK 的 examples 目录下其实是有的，而且在工程的默认配置选项里可以看到 mqtt 的配置，所以应该添加对应的库就能直接使用，直接把例程的代码复制过来，替换前一节 httpss_request 的例程即可

1. MQTT的使用并不复杂，从例程的代码结构也也看出来
- 1.1.首先在主函数里面初始化启动必须的外设，然后初始化mqtt

- 1.2.mqtt的初始化主要涉及一个重要的结构体 `esp_mqtt_client_config_t`，和一个关键的消息回调函数`esp_mqtt_client_register_event`，完成这个的点的设置和实现，就能顺利使用mqtt

- 1.3.消息回调函数会返回各种类型的消息事件，如连接状态，推送消息反馈，订阅消息通知等，用户可以根据自己需要实现相应的处理



## 修改例程适配 ONENET 云
1. 打开配置界面，修改例程配置里，WiFI 的账户密码，这里注意有些 mqtt 的服务器不支持 websocket 的方式，记得反选，如果没用加密项也需要反选

1. 修改需要连接的服务器地址端口等，client_id，username，password 都是根据你在 ONENET 平台建立的产品来的，具体可以去看 onenet 的官方文档

1. 修改处理信息的回调函数，例程是在连接成功消息内，订阅和发布消息，我们测试就在这里简单修改下，适配 onenet 平台的需求

1. 编译运行，输出如下，可以看到服务器已经连接成功，而且收到了订阅的 topic 信息，错误是因为，平台需要发送的信息需要走 json 格式，我这里发送的信息不符合格式


1. 以上就是 mqtt 的使用

# chapter5

板卡带了 LCD 屏幕的扩展版，可以移植一个 LVGL 做图形化界面，而且 LVGL 官方提供了 ESP32 的移植接口，可以从 [lvgl github 地址](https://github.com/lvgl/lv_port_esp32) 下载到源码，但是因为 ESP32S 和 ESP32 的 SPI 外设是有差异的，需要进行相应的修改，spi 驱动例程可以从板卡的[例程](https://github.com/espressif/esp-dev-kits)获取

1. 准备一个简单的工程，可以选择 hello world 例程，把 [lv_port_esp32](https://github.com/lvgl/lv_port_esp32)添加到工程路径，我新建了一个 components 文件夹存放，拷贝 main 文件下的 main.c 替换工程 hello world 下的 main.c, 然后删除了里面 main 文件夹和一些不相关的文件，剩余工程结构如下

1. 修改工程路径下的 CMakeLists.txt， 添加 lvgl 相关文件路径和板卡的路径，按自己实际存放位置修改

1. 修改工程 main 文件夹下的 CMakeLists.txt，添加 LVGL 及板卡的模块注册

1. 修改工程 main 文件夹下 Kconfig.projbuild，加入 LCD 驱动的配置

1. 保存，打开 GUI 配置界面，可以看到 LVGL 和 LCD 扩展板的配置界面，说明模块已经添加成功，可以添加头文件的方式调用相关接口，暂时对 LVGL 的配置不是特别熟悉，就先保留默认配置

1. 如上配置之后，编译的时候还是会失败，还需要修改几个文件， lvgl_esp32_drivers 目录下的 lvgl_help.c 文件，同目录下的 lvgl_tft 文件下的 disp_driver.c 文件，屏蔽相关文件实现即可，具体修改如图

1. 修改 main 文件下的 LCD 初始化函数，用板卡 LCD 例程下的初始化替换即可

1. LVGL 的绘图是通过 frambuff 的方式实现，所以移植的时候其实只需要修改刷屏函数即可 

1. 修改 main 的线程创建，就修改完成了

1. 编译，运行，可以看到效果


1. 以上就是 lvgl 的移植工作，因为 LVGL 官方提供了 ESP32 的参考，所以对我第一次使用的我来说有一定帮助，当然也是因为 ESP32S2 的一些差异性，移植过程也是遇到了很多困难，好在最终结果是光明的。




# chapter6

# onenet
# token setting

| 参数 | 描述 | 示例 |
| -- | -- | -- |
| res | products/` products_id`/devices/`device_name` | products/306708/devices/MTU_32F407 |
| et | utc | 1640966400 |
| key | `products_key` | Qyokom3ShxfDMoGJ5fivy/pmSQj6duiPfctPpAlQo+M= |
| token | sign | version=2018-10-31&res=products%2F306708%2Fdevices%2FMTU_32F407&et=1640966400&method=md5&sign=OZTXRObIiVhuKrESxlY8AA%3D%3D |


# mqtt connect

| 参数 | 描述 | 示例 |
| -- | -- | -- |
| address | 服务器地址 | 183.230.40.16 |
| port | 服务器端口 | 8883 |
| client_id | 设备名 | MTU_32F407 |
| username | 产品 ID | 306708 |
| password | token->sign | version=2018-10-31&res=products%2F306708%2Fdevices%2FMTU_32F407&et=1640966400&method=md5&sign=OZTXRObIiVhuKrESxlY8AA%3D%3D |
| ca cert | 证书 | [certificate.pem](certificate.pem) |

# mqtt subscribe topic
## publish respond topic
$sys/306708/MTU_32F407/dp/post/json/+

## control command topic
$sys/306708/MTU_32F407/cmd/#

# mqtt pubish topic
## pub topic
$sys/306708/MTU_32F407/dp/post/json

## cmd respond topic
$sys/306708/MTU_32F407/cmd/response/c8aa816b-1c4e-40ad-8d8d-038535d8e1a3

# 收发流程
## 推送数据流
1. 推送数据流 eg: $sys/306708/MTU_32F407/dp/post/json
1. 接受反馈 eg: $sys/306708/MTU_32F407/dp/post/json/accepted

## 接受数据流
1. 接收指令 eg: $sys/306708/MTU_32F407/cmd/request/c8aa816b-1c4e-40ad-8d8d-038535d8e1a3
1. 推送反馈 eg: $sys/306708/MTU_32F407/cmd/response/c8aa816b-1c4e-40ad-8d8d-038535d8e1a3
1. 反馈推送反馈信息 eg: $sys/238322/mqtts-test-device/cmd/response/33ffea0a-e5f1-49d6-a626-ffee1bbd93ef/accepted