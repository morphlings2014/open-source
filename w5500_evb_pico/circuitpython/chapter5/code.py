# SPDX-FileCopyrightText: 2024 id.loda
# SPDX-License-Identifier: MIT

import os
import gc
import board
import busio
import digitalio
import displayio
import sdcardio
import storage
import rtc
import terminalio
import time

# Starting in CircuitPython 9.x fourwire will be a seperate internal library
# rather than a component of the displayio library
try:
    from fourwire import FourWire
except ImportError:
    from displayio import FourWire
from adafruit_display_text import label
from adafruit_st7789 import ST7789
from adafruit_bitmap_font import bitmap_font
import adafruit_imageload

from adafruit_wiznet5k.adafruit_wiznet5k import WIZNET5K
import adafruit_wiznet5k.adafruit_wiznet5k_socket as socket
import adafruit_ntp


UTC_OFFSET = 8
WEEK = ["一", "二", "三", "四", "五", "六", "日"]
MONTH = [
    "",
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
]

BUTTON_X = const(6)
BUTTON_Y = const(2)
BUTTON_A = const(5)
BUTTON_B = const(1)
BUTTON_SELECT = const(0)
BUTTON_START = const(16)
button_mask = const(
    (1 << BUTTON_X)
    | (1 << BUTTON_Y)
    | (1 << BUTTON_A)
    | (1 << BUTTON_B)
    | (1 << BUTTON_SELECT)
    | (1 << BUTTON_START)
)

# SPI0 for w5x00
SPI0_SCK = board.GP18
SPI0_TX = board.GP19
SPI0_RX = board.GP16
SPI0_CSn = board.GP17

# w5x00 reset
W5x00_RSTn = board.GP20

# SPI1 for tft
SPI1_SCK = board.GP10
SPI1_TX = board.GP11
SPI1_RX = board.GP12

# tft lcd io
TFT_CSn = board.GP13
TFT_RSTn = board.GP14
TFT_DC = board.GP15

# sdcard io
SD_CSn = board.GP9


led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

eth_rst = digitalio.DigitalInOut(W5x00_RSTn)
eth_rst.direction = digitalio.Direction.OUTPUT
eth_cs = digitalio.DigitalInOut(SPI0_CSn)
spi0_bus = busio.SPI(SPI0_SCK, MOSI=SPI0_TX, MISO=SPI0_RX)

# Reset W5500 first
eth_rst.value = False
time.sleep(1)
eth_rst.value = True

# Initialize ethernet interface with DHCP
# eth = WIZNET5K(spi0_bus, eth_cs)
# Initialize ethernet interface without DHCP
eth = WIZNET5K(spi0_bus, eth_cs, is_dhcp=False)

# Setup your network configuration below
IP_ADDRESS = (192, 168, 0, 99)  # (10, 38, 180, 221)
SUBNET_MASK = (255, 255, 255, 0)  # (255, 255, 254, 0)
GATEWAY_ADDRESS = (192, 168, 0, 1)  # (10, 38, 180, 1)
DNS_SERVER = (211, 136, 150, 66)  # (10, 38, 116, 23)
eth.ifconfig = (IP_ADDRESS, SUBNET_MASK, GATEWAY_ADDRESS, DNS_SERVER)

# Release any resources currently in use for the displays
displayio.release_displays()
spi1_bus = busio.SPI(SPI1_SCK, MOSI=SPI1_TX, MISO=SPI1_RX)

display_bus = FourWire(spi1_bus, command=TFT_DC, chip_select=TFT_CSn, reset=TFT_RSTn)
display = ST7789(display_bus, width=320, height=240, rotation=90)

sdcard = sdcardio.SDCard(spi1_bus, SD_CSn)
vfs = storage.VfsFat(sdcard)
storage.mount(vfs, "/sd")

# Make the display context
splash = displayio.Group()
display.root_group = splash

color_bitmap = displayio.Bitmap(320, 240, 1)
color_palette = displayio.Palette(1)
color_palette[0] = 0x000000  # 0xF8F8FF  # Ghost White

bg_sprite = displayio.TileGrid(color_bitmap, pixel_shader=color_palette, x=0, y=0)
splash.append(bg_sprite)
# 180 * 125
with open("/sd/resources/pic/test_image.png", "rb") as f:
    blinka_bitmap, blinka_palette = adafruit_imageload.load(
        f, bitmap=displayio.Bitmap, palette=displayio.Palette
    )
blinka_sprite = displayio.TileGrid(
    blinka_bitmap, pixel_shader=blinka_palette, x=140, y=115
)
splash.append(blinka_sprite)

text_group = displayio.Group()  # (scale=1, x=10, y=10)
font = bitmap_font.load_font("/sd/resources/font/opposans_m_12.pcf")
time_area = label.Label(font, x=10, y=12, text="", scale=1, color=0x00BFFF)
text_group.append(time_area)
info_area = label.Label(terminalio.FONT, x=10, y=32, text="", scale=1, color=0xFFFF00)
text_group.append(info_area)  # Subgroup for text scaling
text_area = label.Label(terminalio.FONT, x=10, y=120, text="", scale=1, color=0xF8F8FF)
text_group.append(text_area)  # Subgroup for text scaling
splash.append(text_group)

# code handler
ifconfig = eth.ifconfig
info_area.text = info_area.text + f"mac addr: {eth.pretty_mac(eth.mac_address)}\n"
info_area.text = info_area.text + f"ip4 addr: {eth.pretty_ip(ifconfig[0])}\n"
info_area.text = info_area.text + f"subnet addr: {eth.pretty_ip(ifconfig[1])}\n"
info_area.text = info_area.text + f"gateway addr: {eth.pretty_ip(ifconfig[2])}\n"
info_area.text = info_area.text + f"dns addr: {eth.pretty_ip(ifconfig[3])}\n"
domain_address = eth.get_host_by_name("eeworld.com.cn")
info_area.text = info_area.text + f"eeworld addr: {eth.pretty_ip(domain_address)}\n"
print(info_area.text)
# print(f"max sockets:{eth.max_sockets}")
text_area.text = f"max sockets:{eth.max_sockets}\n"

# Initialize a socket for our server
socket.set_interface(eth)


def ntp_sync_time():
    print("Local time before synchronization:%s" % str(time.localtime()))
    try:
        # ntp = adafruit_ntp.NTP(socket, tz_offset=UTC_OFFSET)
        ntp = adafruit_ntp.NTP(socket, server="ntp.aliyun.com", tz_offset=UTC_OFFSET)
        # NOTE: This changes the system time so make sure you aren't assuming that time
        # doesn't jump.
        rtc.RTC().datetime = ntp.datetime
        print("Local time after synchronization: %s" % str(time.localtime()))
    except OSError as e:
        print("ntp error:{}".format(e))


ntp_sync_time()
date = time.localtime()
time_area.text = "{}/{:0>2}/{:0>2} {:0>2}:{:0>2}:{:0>2} 星期{}".format(
    date[0], date[1], date[2], date[3], date[4], date[5], WEEK[date[6]]
)


def cplog(log):
    print(log)
    text_area.text = text_area.text + log + "\n"


def cplog_clear():
    text_area.text = ''


# compare fname against pattern. Pattern may contain
# wildcards ? and *.
def fncmp(fname, pattern):
    pi = 0
    si = 0
    while pi < len(pattern) and si < len(fname):
        if (fname[si] == pattern[pi]) or (pattern[pi] == "?"):
            si += 1
            pi += 1
        else:
            if pattern[pi] == "*":  # recurse
                if (pi + 1) == len(pattern):
                    return True
                while si < len(fname):
                    if fncmp(fname[si:], pattern[pi + 1 :]):
                        return True
                    else:
                        si += 1
                return False
            else:
                return False
    if pi == len(pattern.rstrip("*")) and si == len(fname):
        return True
    else:
        return False


def get_absolute_path(cwd, payload):
    # Just a few special cases "..", "." and ""
    # If payload start's with /, set cwd to /
    # and consider the remainder a relative path
    if payload.startswith("/"):
        cwd = "/"
    for token in payload.split("/"):
        if token == "..":
            if cwd != "/":
                cwd = "/".join(cwd.split("/")[:-1])
                if cwd == "":
                    cwd = "/"
        elif token != "." and token != "":
            if cwd == "/":
                cwd += token
            else:
                cwd = cwd + "/" + token
    return cwd


def make_description(path, fname, full):
    if full:
        stat = os.stat(get_absolute_path(path, fname))
        file_permissions = (
            "drwxr-xr-x" if (stat[0] & 0o170000 == 0o040000) else "-rw-r--r--"
        )
        file_size = stat[6]
        tm = time.localtime(stat[7])
        if tm[0] != time.localtime()[0]:
            description = "{}    1 owner group {:>10} {} {:2} {:>5} {}\r\n".format(
                file_permissions, file_size, MONTH[tm[1]], tm[2], tm[0], fname
            )
        else:
            description = (
                "{}    1 owner group {:>10} {} {:2} {:02}:{:02} {}\r\n".format(
                    file_permissions,
                    file_size,
                    MONTH[tm[1]],
                    tm[2],
                    tm[3],
                    tm[4],
                    fname,
                )
            )
    else:
        description = fname + "\r\n"
    return description


def send_list_data(path, dataclient, full):
    try:  # whether path is a directory name
        for fname in os.listdir(path):
            description = make_description(path, fname, full)
            print(description)
            if dataclient:
                dataclient.send(description)
    except Exception as err:
        print(f"list err:{err}")  # path may be a file name or pattern
        pattern = path.split("/")[-1]
        path = path[: -(len(pattern) + 1)]
        if path == "":
            path = "/"
        for fname in os.listdir(path):
            if fncmp(fname, pattern):
                description = make_description(path, fname, full)
                print(description)
                if dataclient:
                    dataclient.send(description)


def send_file_data(path, dataclient):
    with open(path, "r") as file:
        chunk = file.read(512)
        while len(chunk) > 0:
            dataclient.send(chunk)
            chunk = file.read(512)


def save_file_data(path, dataclient, mode):
    with open(path, mode) as file:
        try:
            buffer = bytearray(512)
            length = dataclient.recv_into(buffer, len(buffer))
            while length > 0:
                file.write(buffer[0:length])
                if dataclient._available() > 0:
                    length = dataclient.recv_into(buffer, len(buffer))
                else:
                    break
            # chunk = dataclient.recv(512)
            # while len(chunk) > 0:
            #     file.write(chunk)
            #     chunk = dataclient.recv(512)
        except Exception as err:
            print(f"file save err:{err}")


def socket_readline(sock):
    line = b""
    char = bytearray(1)
    while True:
        length = sock.recv_into(char, len(char))
        if length > 0:
            if char == b"\n":
                break
            line += char

    # while True:
    #     char = sock.recv(1)
    #     if char == b'\n':
    #         break
    #     line += char
    return line


def ftpserver(netif, socketpool, ipaddr="", port=21, timeout=None):

    DATA_PORT = 13333

    ftpsocket = socketpool.socket(socketpool.AF_INET, socketpool.SOCK_STREAM)
    datasocket = socketpool.socket(socketpool.AF_INET, socketpool.SOCK_STREAM)

    # ftpsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # datasocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # ftpsocket.bind(socketpool.getaddrinfo("0.0.0.0", 21)[0][4])
    # datasocket.bind(socketpool.getaddrinfo("0.0.0.0", DATA_PORT)[0][4])
    ftpsocket.bind((ipaddr, port))
    datasocket.bind((ipaddr, DATA_PORT))

    ftpsocket.listen()
    datasocket.listen()
    datasocket.settimeout(timeout)

    msg_250_OK = "250 OK\r\n"
    msg_550_fail = "550 Failed\r\n"

    cplog(f"FTP Server started on {ipaddr}:{port}")
    try:
        dataclient = None
        fromname = None
        while True:
            cl, remote_addr = ftpsocket.accept()
            cl.settimeout(100)
            cwd = "/"
            try:
                cplog(f"FTP connection from:{remote_addr}")
                cl.send("220 Hello, this is the W5500_EVB_PICO.\r\n")
                while True:
                    gc.collect()
                    data = cl._readline().decode("utf-8").rstrip("\r\n")
                    # data = socket_readline(cl).decode("utf-8").rstrip("\r\n")
                    if len(data) <= 0:
                        print("Client disappeared")
                        break

                    command = data.split(" ")[0].upper()
                    payload = data[len(command) :].lstrip()

                    path = get_absolute_path(cwd, payload)

                    print(
                        "Command={}, Payload={}, Path={}".format(command, payload, path)
                    )

                    if command == "USER":
                        # If you want to see a password,return
                        #   "331 Need password.\r\n" instead
                        # If you want to reject an user, return
                        #   "530 Not logged in.\r\n"
                        cl.send("230 Logged in.\r\n")
                    elif command == "PASS":
                        # you may check here for a valid password and return
                        # "530 Not logged in.\r\n" in case it's wrong
                        # self.logged_in = True
                        cl.send("230 Logged in.\r\n")
                    elif command == "SYST":
                        cl.send("215 UNIX Type: L8\r\n")
                    elif command in ("NOOP", "ABOR"):
                        # just accept & ignore
                        cl.send("200 OK\r\n")
                    elif command == "FEAT":
                        cl.send("211 no-features\r\n")
                    elif command == "PWD" or command == "XPWD":
                        cl.send('257 "{}"\r\n'.format(cwd))
                    elif command == "CWD" or command == "XCWD":
                        try:
                            files = os.listdir(path)
                            cwd = path
                            cl.send(msg_250_OK)
                        except Exception as err:
                            cplog(f"command {command} err:{err}")
                            cl.send(msg_550_fail)
                    elif command == "CDUP":
                        cwd = get_absolute_path(cwd, "..")
                        cl.send(msg_250_OK)
                    elif command == "TYPE":
                        # probably should switch between binary and not
                        cl.send("200 Transfer mode set\r\n")
                    elif command == "SIZE":
                        try:
                            size = os.stat(path)[6]
                            cl.send("213 {}\r\n".format(size))
                        except Exception as err:
                            cplog(f"command {command} err:{err}")
                            cl.send(msg_550_fail)
                    elif command == "QUIT":
                        cl.send("221 Bye.\r\n")
                        break
                    elif command == "PASV":
                        addr = ipaddr
                        cl.send(
                            "227 Entering Passive Mode ({},{},{}).\r\n".format(
                                addr.replace(".", ","), DATA_PORT >> 8, DATA_PORT % 256
                            )
                        )
                        dataclient, data_addr = datasocket.accept()
                        dataclient.settimeout(1)
                        cplog(f"FTP Data connection from:{data_addr}")
                    elif command == "PORT":
                        items = payload.split(",")
                        if len(items) >= 6:
                            data_addr = ".".join(items[:4])
                            if data_addr == "127.0.1.1":
                                # replace by command session addr
                                data_addr = remote_addr
                            DATA_PORT = int(items[4]) * 256 + int(items[5])
                            cl.send("200 OK\r\n")
                        else:
                            cl.send("504 Fail\r\n")
                    elif command == "LIST" or command == "NLST":
                        if not payload.startswith("-"):
                            place = path
                        else:
                            place = cwd
                        try:
                            cl.send("150 Here comes the directory listing.\r\n")
                            send_list_data(
                                place, dataclient, command == "LIST" or payload == "-l"
                            )
                            cl.send("226 Listed.\r\n")
                        except Exception as err:
                            cplog(f"command {command} err:{err}")
                            cl.send(msg_550_fail)
                        if dataclient is not None:
                            # dataclient.close()
                            dataclient._disconnect()
                            dataclient = None
                    elif command == "RETR":
                        try:
                            cl.send("150 Opening data connection.\r\n")
                            send_file_data(path, dataclient)
                            cl.send("226 Transfer complete.\r\n")
                        except Exception as err:
                            cplog(f"command {command} err:{err}")
                            cl.send(msg_550_fail)
                        if dataclient is not None:
                            # dataclient.close()
                            dataclient._disconnect()
                            dataclient = None
                    elif command == "STOR":
                        try:
                            cl.send("150 Ok to send data.\r\n")
                            save_file_data(path, dataclient, "w")
                            cl.send("226 Transfer complete.\r\n")
                        except Exception as err:
                            cplog(f"command {command} err:{err}")
                            cl.send(msg_550_fail)
                        if dataclient is not None:
                            # dataclient.close()
                            dataclient._disconnect()
                            dataclient = None
                    elif command == "APPE":
                        try:
                            cl.send("150 Ok to send data.\r\n")
                            save_file_data(path, dataclient, "a")
                            cl.send("226 Transfer complete.\r\n")
                        except Exception as err:
                            cplog(f"command {command} err:{err}")
                            cl.send(msg_550_fail)
                        if dataclient is not None:
                            # dataclient.close()
                            dataclient._disconnect()
                            dataclient = None
                    elif command == "DELE":
                        try:
                            os.remove(path)
                            cl.send(msg_250_OK)
                        except Exception as err:
                            cplog(f"command {command} err:{err}")
                            cl.send(msg_550_fail)
                    elif command == "RMD" or command == "XRMD":
                        try:
                            os.rmdir(path)
                            cl.send(msg_250_OK)
                        except Exception as err:
                            cplog(f"command {command} err:{err}")
                            cl.send(msg_550_fail)
                    elif command == "MKD" or command == "XMKD":
                        try:
                            os.mkdir(path)
                            cl.send(msg_250_OK)
                        except Exception as err:
                            cplog(f"command {command} err:{err}")
                            cl.send(msg_550_fail)
                    elif command == "RNFR":
                        fromname = path
                        cl.send("350 Rename from\r\n")
                    elif command == "RNTO":
                        if fromname is not None:
                            try:
                                os.rename(fromname, path)
                                cl.send(msg_250_OK)
                            except Exception as err:
                                cplog(f"command {command} err:{err}")
                                cl.send(msg_550_fail)
                        else:
                            cl.send(msg_550_fail)
                        fromname = None
                    elif command == "MDTM":
                        try:
                            tm = time.localtime(os.stat(path)[8])
                            cl.send(
                                "213 {:04d}{:02d}{:02d}{:02d}{:02d}{:02d}\r\n".format(
                                    *tm[0:6]
                                )
                            )
                        except Exception as err:
                            cplog(f"command {command} err:{err}")
                            cl.send("550 Fail\r\n")
                    elif command == "STAT":
                        if payload == "":
                            cl.send(
                                "211-Connected to ({})\r\n"
                                "    Data address ({})\r\n"
                                "211 TYPE: Binary STRU: File MODE:"
                                " Stream\r\n".format(remote_addr[0], addr)
                            )
                        else:
                            cl.send("213-Directory listing:\r\n")
                            send_list_data(path, cl, True)
                            cl.send("213 Done.\r\n")
                    elif command == "SITE":
                        try:
                            # exec(payload.replace("\0", "\n"))
                            cl.send("250 OK\r\n")
                        except Exception as err:
                            cplog(f"command {command} err:{err}")
                            cl.send("550 Fail\r\n")
                    else:
                        cl.send("502 Unsupported command.\r\n")
                        print(
                            "Unsupported command {} with payload {}".format(
                                command, payload
                            )
                        )
            except Exception as err:
                cplog(f"err:{err}")

            finally:
                cl.close()
                cl = None
            break
    finally:
        datasocket.close()
        ftpsocket.close()
        if dataclient is not None:
            dataclient.close()


# send_list_data("/", None, True)
ftpserver(
    netif=eth,
    socketpool=socket,
    ipaddr=eth.pretty_ip(eth.ip_address),
    timeout=500,
)
cplog_clear()

print("Done!")
