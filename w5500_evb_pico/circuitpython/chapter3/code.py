# SPDX-FileCopyrightText: 2024 id.loda
# SPDX-License-Identifier: MIT

import board
import busio
import digitalio
import displayio
import sdcardio
import storage
import terminalio
import time
# Starting in CircuitPython 9.x fourwire will be a seperate internal library
# rather than a component of the displayio library
try:
    from fourwire import FourWire
except ImportError:
    from displayio import FourWire
from adafruit_display_text import label
from adafruit_st7789 import ST7789
from adafruit_bitmap_font import bitmap_font
import adafruit_imageload

from adafruit_wiznet5k.adafruit_wiznet5k import WIZNET5K
import adafruit_wiznet5k.adafruit_wiznet5k_socket as socket


# SPI0 for w5x00
SPI0_SCK = board.GP18
SPI0_TX = board.GP19
SPI0_RX = board.GP16
SPI0_CSn = board.GP17

## w5x00 reset
W5x00_RSTn = board.GP20

# SPI1 for tft
SPI1_SCK = board.GP10
SPI1_TX = board.GP11
SPI1_RX = board.GP12

# tft lcd io
TFT_CSn = board.GP13
TFT_RSTn = board.GP14
TFT_DC = board.GP15

# sdcard io
SD_CSn = board.GP9


led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

# Release any resources currently in use for the displays
displayio.release_displays()
spi1_bus = busio.SPI(SPI1_SCK, MOSI=SPI1_TX, MISO=SPI1_RX)

display_bus = FourWire(spi1_bus, command=TFT_DC, chip_select=TFT_CSn, reset=TFT_RSTn)
display = ST7789(display_bus, width=320, height=240, rotation=90)

sdcard = sdcardio.SDCard(spi1_bus, SD_CSn)
vfs = storage.VfsFat(sdcard)
storage.mount(vfs, "/sd")

# Make the display context
splash = displayio.Group()
display.root_group = splash

color_bitmap = displayio.Bitmap(320, 240, 1)
color_palette = displayio.Palette(1)
color_palette[0] = 0x000000 # 0xF8F8FF  # Ghost White

bg_sprite = displayio.TileGrid(color_bitmap, pixel_shader=color_palette, x=0, y=0)
splash.append(bg_sprite)
# 180 * 125
with open("/sd/resources/pic/test_image.png", "rb") as f:
    blinka_bitmap, blinka_palette = adafruit_imageload.load(f, bitmap=displayio.Bitmap, palette=displayio.Palette)
blinka_sprite = displayio.TileGrid(blinka_bitmap, pixel_shader=blinka_palette, x=140, y=115)
splash.append(blinka_sprite)

text_group = displayio.Group(scale=1, x=10, y=10)
text_area = label.Label(terminalio.FONT, text="", color=0xFFFF00)
text_group.append(text_area) # Subgroup for text scaling
splash.append(text_group)

# code handler
eth_rst = digitalio.DigitalInOut(W5x00_RSTn)
eth_rst.direction = digitalio.Direction.OUTPUT
eth_cs = digitalio.DigitalInOut(SPI0_CSn)
spi0_bus = busio.SPI(SPI0_SCK, MOSI=SPI0_TX, MISO=SPI0_RX)

# Reset W5500 first
eth_rst.value = False
time.sleep(1)
eth_rst.value = True

# Initialize ethernet interface with DHCP
# eth = WIZNET5K(spi0_bus, eth_cs)
# Initialize ethernet interface without DHCP
eth = WIZNET5K(spi0_bus, eth_cs, is_dhcp=False)

# # Setup your network configuration below
IP_ADDRESS = (192, 168, 0, 3) #(10, 38, 180, 221)
SUBNET_MASK = (255, 255, 255, 0) # (255, 255, 254, 0)
GATEWAY_ADDRESS = (192, 168, 0, 1) # (10, 38, 180, 1)
DNS_SERVER = (211, 136, 150, 66) # (10, 38, 116, 23)
eth.ifconfig = (IP_ADDRESS, SUBNET_MASK, GATEWAY_ADDRESS, DNS_SERVER)

ifconfig = eth.ifconfig
text_area.text = text_area.text + "mac addr: {}\n".format(eth.pretty_mac(eth.mac_address))
text_area.text = text_area.text + "ip4 addr: {}\n".format(eth.pretty_ip(ifconfig[0]))
text_area.text = text_area.text + "subnet addr: {}\n".format(eth.pretty_ip(ifconfig[1]))
text_area.text = text_area.text + "gateway addr: {}\n".format(eth.pretty_ip(ifconfig[2]))
text_area.text = text_area.text + "dns addr: {}\n".format(eth.pretty_ip(ifconfig[3]))
print(text_area.text)

# Initialize a socket for our server
socket.set_interface(eth)
# socket.setdefaulttimeout(50)
server = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)  # Allocate socket for the server
server_ip = eth.pretty_ip(eth.ip_address)  # IP address of server
server_port = 5000  # Port to listen on
server.bind((server_ip, server_port))  # Bind to IP and Port
server.listen()  # Begin listening for incoming clients
# print("socket connected")

# while True:
#     led.value = not led.value
#     time.sleep(1)
#     with server:
#         data = "Welcome to w5500 evb pico's udp server"
#         server.sendto(data.encode(),(server_ip, server_port))  # Echo message back to client
#         # data = conn.recv()
#         # print(data)
#         # conn.send(data)  # Echo message back to client
#         while True:
#             data, addr = server.recvfrom(10)
#             print(f"data from IP:{addr[0]} Port:{addr[1]}")
#             if data:
#                 print(f"recv data: {data}")
#                 text_area.text = text_area.text + f"recv data: {addr}\n"
#                 server.sendto(data,(server_ip, server_port))  # Echo message back to client
#                 break
#     break

while True:
    print(f"Accepting connections on {server_ip}:{server_port}")
    text_area.text = text_area.text + "Accepting connections on {}:{}\n".format(server_ip, server_port)
    conn, addr = server.accept()  # Wait for a connection from a client.
    led.value = True
    print(f"Connection accepted from {addr}, reading exactly 1024 bytes from client")
    text_area.text = text_area.text + "Connection accepted from {}\n".format(addr)
    conn.settimeout(100)
    conn.send("Welcome to w5500 evb pico's tcp server")
    with conn:
        buffer = bytearray(1024)
        length = conn.recv_into(buffer, len(buffer))
        if length > 0:
            data = str(buffer[0:length], "utf-8")
            text_area.text = text_area.text + f"recv data: {data}\n"
            conn.send(data)  # Echo message back to client
        if data:  # Wait for receiving data
            print(f"recv data: {data}")
            text_area.text = text_area.text + f"recv data: {data}\n"
            conn.send(data)  # Echo message back to client
    led.value = False
    print("Connection closed")

print("Done!")
