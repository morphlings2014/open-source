# SPDX-FileCopyrightText: 2024 id.loda
# SPDX-License-Identifier: MIT

import board
import busio
import digitalio
import displayio
import sdcardio
import storage
import terminalio
import time
# Starting in CircuitPython 9.x fourwire will be a seperate internal library
# rather than a component of the displayio library
try:
    from fourwire import FourWire
except ImportError:
    from displayio import FourWire
from adafruit_display_text import label
from adafruit_st7789 import ST7789
from adafruit_bitmap_font import bitmap_font
import adafruit_imageload

import socketpool
import wifi


# wifi
SSID = "ASR-Mobile-SW"  # "shanghai2014"
PASSWORD = "ROOZLrCAFcXr2lv0"  # "zafu0828.samw"

# SPI1 for tft
SPI1_SCK = board.GP10
SPI1_TX = board.GP11
SPI1_RX = board.GP12

# tft lcd io
TFT_CSn = board.GP13
TFT_RSTn = board.GP14
TFT_DC = board.GP15

# sdcard io
SD_CSn = board.GP9


led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

# Release any resources currently in use for the displays
displayio.release_displays()
spi1_bus = busio.SPI(SPI1_SCK, MOSI=SPI1_TX, MISO=SPI1_RX)

display_bus = FourWire(spi1_bus, command=TFT_DC, chip_select=TFT_CSn, reset=TFT_RSTn)
display = ST7789(display_bus, width=320, height=240, rotation=90)

sdcard = sdcardio.SDCard(spi1_bus, SD_CSn)
vfs = storage.VfsFat(sdcard)
storage.mount(vfs, "/sd")

# Make the display context
splash = displayio.Group()
display.root_group = splash

color_bitmap = displayio.Bitmap(320, 240, 1)
color_palette = displayio.Palette(1)
color_palette[0] = 0x000000 # 0xF8F8FF  # Ghost White

bg_sprite = displayio.TileGrid(color_bitmap, pixel_shader=color_palette, x=0, y=0)
splash.append(bg_sprite)
# 180 * 125
with open("/sd/resources/pic/test_image.png", "rb") as f:
    blinka_bitmap, blinka_palette = adafruit_imageload.load(f, bitmap=displayio.Bitmap, palette=displayio.Palette)
blinka_sprite = displayio.TileGrid(blinka_bitmap, pixel_shader=blinka_palette, x=140, y=115)
splash.append(blinka_sprite)

text_group = displayio.Group(scale=1, x=10, y=10)
text_area = label.Label(terminalio.FONT, text="", color=0xFFFF00)
text_group.append(text_area) # Subgroup for text scaling
splash.append(text_group)

# code handler
while not wifi.radio.ipv4_address:
    try:
        wifi.radio.connect(SSID, PASSWORD)
    except ConnectionError as e:
        print("connect error:{}, retry in 2s".format(e))
    time.sleep(2)
pool = socketpool.SocketPool(wifi.radio)

# mac type is ReadableBuffer
import binascii
mac_address = binascii.hexlify(wifi.radio.mac_address).decode('utf-8').upper()
formatted_mac = ':'.join([mac_address[i:i+2] for i in range(0, len(mac_address), 2)])
text_area.text = text_area.text + f"mac addr: {formatted_mac}\n"
text_area.text = text_area.text + f"ip4 addr: {wifi.radio.ipv4_address}\n"
text_area.text = text_area.text + f"subnet addr: {wifi.radio.ipv4_subnet}\n"
text_area.text = text_area.text + f"gateway addr: {wifi.radio.ipv4_gateway}\n"
text_area.text = text_area.text + f"dns addr: {wifi.radio.ipv4_dns}\n"
print(text_area.text)

# Initialize a socket for our server
server = pool.socket()  # Allocate socket for the server
server_ip = str(wifi.radio.ipv4_address)  # IP address of server
server_port = 50007  # Port to listen on
server.bind((server_ip, server_port))  # Bind to IP and Port
server.listen(0)  # Begin listening for incoming clients

while True:
    print(f"Accepting connections on {server_ip}:{server_port}")
    text_area.text = text_area.text + f"Accepting connections on {server_ip}:{server_port}\n"
    conn, addr = server.accept()  # Wait for a connection from a client.
    led.value = True
    print(f"Connection accepted from {addr}, reading exactly 1024 bytes from client")
    text_area.text = text_area.text + f"Connection accepted from {addr}\n"
    conn.send("Welcome to w5500 evb pico's tcp server")
    recv_buffer = bytearray(1024)
    with conn:
         while True:
            length, (addr, port) = conn.recvfrom_into(recv_buffer)
            if length > 0:  # Wait for receiving data
                recv_str = str(recv_buffer, 'utf-8')
                print(f"recv from {addr}:{port} {length} bytes data: {recv_str}")
                text_area.text = text_area.text + f"recv: {recv_str}\n"
                conn.send(recv_buffer)  # Echo message back to client
    led.value = False
    print("Connection closed")

print("Done!")
