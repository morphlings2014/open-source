# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-FileCopyrightText: 2023 Kattni Rembor for Adafruit Industries

# SPDX-License-Identifier: MIT

import os
import time
import board
import busio
import digitalio
import displayio
import rtc
import sdcardio
import storage
import terminalio
import socketpool
import wifi

# Starting in CircuitPython 9.x fourwire will be a seperate internal library
# rather than a component of the displayio library
try:
    from fourwire import FourWire
except ImportError:
    from displayio import FourWire

from micropython import const
from adafruit_display_text import label
from adafruit_st7789 import ST7789
import adafruit_imageload
import adafruit_ntp
from adafruit_seesaw.seesaw import Seesaw

SSID = "ASR-Mobile-SW"  # "shanghai2014"
PASSWORD = "ROOZLrCAFcXr2lv0"  # "zafu0828.samw"
UTC_OFFSET = 8

BUTTON_X = const(6)
BUTTON_Y = const(2)
BUTTON_A = const(5)
BUTTON_B = const(1)
BUTTON_SELECT = const(0)
BUTTON_START = const(16)
button_mask = const(
    (1 << BUTTON_X)
    | (1 << BUTTON_Y)
    | (1 << BUTTON_A)
    | (1 << BUTTON_B)
    | (1 << BUTTON_SELECT)
    | (1 << BUTTON_START)
)

# SPI1 for tft and sdcard
SPI1_SCK = board.GP10
SPI1_TX = board.GP11
SPI1_RX = board.GP12

# tft lcd io
TFT_CSn = board.GP13
TFT_RSTn = board.GP14
TFT_DC = board.GP15
# sdcard io
SD_CSn = board.GP9

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

# Release any resources currently in use for the displays
displayio.release_displays()
spi1_bus = busio.SPI(SPI1_SCK, MOSI=SPI1_TX, MISO=SPI1_RX)
display_bus = FourWire(spi1_bus, command=TFT_DC, chip_select=TFT_CSn, reset=TFT_RSTn)
display = ST7789(display_bus, width=320, height=240, rotation=90)

sdcard = sdcardio.SDCard(spi1_bus, SD_CSn)
vfs = storage.VfsFat(sdcard)
storage.mount(vfs, "/sd")

# i2c_bus = board.I2C()  # Uses board.SCL and board.SDA. Use with breadboard.
i2c_bus = busio.I2C(scl=board.GP27, sda=board.GP26)
seesaw = Seesaw(i2c_bus, addr=0x50)
seesaw.pin_mode_bulk(button_mask, seesaw.INPUT_PULLUP)

last_x = 0
last_y = 0

def ntp_sync_time():
    print("local time before synchronization:%s" % str(time.localtime()))
    try:
        # ntp = adafruit_ntp.NTP(pool, tz_offset=UTC_OFFSET)
        ntp = adafruit_ntp.NTP(pool, server='ntp.aliyun.com', tz_offset=UTC_OFFSET)
        # ntp = adafruit_ntp.NTP(pool, server='ntp.ntsc.ac.cn', tz_offset=UTC_OFFSET)
        # NOTE: This changes the system time so make sure you aren't assuming that time
        # doesn't jump.
        rtc.RTC().datetime = ntp.datetime
        print("local time after synchronization: %s" % str(time.localtime()))
    except OSError as e:
        print("ntp error:{}".format(e))

# code handler
while not wifi.radio.ipv4_address:
    try:
        wifi.radio.connect(SSID, PASSWORD)
    except ConnectionError as e:
        print("connect error:{}, retry in 2s".format(e))
    time.sleep(2)

print("get ip:", wifi.radio.ipv4_address)
pool = socketpool.SocketPool(wifi.radio)
ntp_sync_time()

def lcd_test():
    # Make the display context
    splash = displayio.Group()
    display.root_group = splash
    # 从SD卡加载图片
    with open("/sd/resources/pic/1.bmp", "rb") as f:
        picture, palette = adafruit_imageload.load(f, bitmap=displayio.Bitmap, palette=displayio.Palette)
    # 创建TileGrid来保存图片
    tile_grid = displayio.TileGrid(picture, pixel_shader=palette)
    # 将TileGrid添加到组中
    splash.append(tile_grid)
    # 显示组
    display.show(splash)

lcd_test()

def file_test():
    import microcontroller
    # open file for append
    with open("/sd/temperature.txt", "a") as f:
        led.value = True  # turn on LED to indicate we're writing to the file
        t = microcontroller.cpu.temperature
        print("Temperature = %0.1f" % t)
        f.write("%0.1f\n" % t)
        led.value = False  # turn off LED to indicate we're done
    # file is saved
    time.sleep(0.01)

def print_directory(path, tabs=0):
    for file in os.listdir(path):
        stats = os.stat(path + "/" + file)
        filesize = stats[6]
        isdir = stats[0] & 0x4000

        if filesize < 1000:
            sizestr = str(filesize) + " by"
        elif filesize < 1000000:
            sizestr = "%0.1f KB" % (filesize / 1000)
        else:
            sizestr = "%0.1f MB" % (filesize / 1000000)

        prettyprintname = ""
        for _ in range(tabs):
            prettyprintname += "   "
        prettyprintname += file
        if isdir:
            prettyprintname += "/"
        print('{0:<40} Size: {1:>10}'.format(prettyprintname, sizestr))

        # recursively print directory contents
        if isdir:
            print_directory(path + "/" + file, tabs + 1)


print("Files on filesystem:")
print("====================")
print_directory("/sd")

def seesaw_test():
    global last_x, last_y

    x = 1023 - seesaw.analog_read(14)
    y = 1023 - seesaw.analog_read(15)

    if (abs(x - last_x) > 3) or (abs(y - last_y) > 3):
        print(x, y)
        last_x = x
        last_y = y

    buttons = seesaw.digital_read_bulk(button_mask)

    if not buttons & (1 << BUTTON_X):
        print("Button x pressed")

    if not buttons & (1 << BUTTON_Y):
        print("Button Y pressed")

    if not buttons & (1 << BUTTON_A):
        print("Button A pressed")

    if not buttons & (1 << BUTTON_B):
        print("Button B pressed")

    if not buttons & (1 << BUTTON_SELECT):
        print("Button Select pressed")

    if not buttons & (1 << BUTTON_START):
        print("Button Start pressed")

    time.sleep(0.01)

while True:
    led.value = not led.value
    time.sleep(0.5)
    seesaw_test()
