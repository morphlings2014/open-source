# SPDX-FileCopyrightText: 2024 id.loda
# SPDX-License-Identifier: MIT

import board
import busio
import digitalio
import displayio
import sdcardio
import storage
import terminalio
import time

# Starting in CircuitPython 9.x fourwire will be a seperate internal library
# rather than a component of the displayio library
try:
    from fourwire import FourWire
except ImportError:
    from displayio import FourWire
from adafruit_display_text import label
from adafruit_st7789 import ST7789
from adafruit_bitmap_font import bitmap_font
import adafruit_imageload


# SPI1 for tft
SPI1_SCK = board.GP10
SPI1_TX = board.GP11
SPI1_RX = board.GP12

# tft lcd io
TFT_CSn = board.GP13
TFT_RSTn = board.GP14
TFT_DC = board.GP15

# sdcard io
SD_CSn = board.GP9


led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

# Release any resources currently in use for the displays
displayio.release_displays()
spi1_bus = busio.SPI(SPI1_SCK, MOSI=SPI1_TX, MISO=SPI1_RX)

display_bus = FourWire(spi1_bus, command=TFT_DC, chip_select=TFT_CSn, reset=TFT_RSTn)
display = ST7789(display_bus, width=320, height=240, rotation=90)

sdcard = sdcardio.SDCard(spi1_bus, SD_CSn)
vfs = storage.VfsFat(sdcard)
storage.mount(vfs, "/sd")

# Make the display context
splash = displayio.Group()
display.root_group = splash

color_bitmap = displayio.Bitmap(320, 240, 1)
color_palette = displayio.Palette(1)
color_palette[0] = 0x000000 # 0xF8F8FF  # Ghost White

bg_sprite = displayio.TileGrid(color_bitmap, pixel_shader=color_palette, x=0, y=0)
splash.append(bg_sprite)
# 160 * 29
with open("/sd/resources/pic/dk.bmp", "rb") as f:
    digikey_bitmap, digikey_palette = adafruit_imageload.load(f, bitmap=displayio.Bitmap, palette=displayio.Palette)
digikey_sprite = displayio.TileGrid(digikey_bitmap, pixel_shader=digikey_palette, x=0, y=34)
splash.append(digikey_sprite)

# 160 * 56
with open("/sd/resources/pic/ee.bmp", "rb") as f:
    eeworld_bitmap, eeworld_palette = adafruit_imageload.load(f, bitmap=displayio.Bitmap, palette=displayio.Palette)
eeworld_sprite = displayio.TileGrid(eeworld_bitmap, pixel_shader=eeworld_palette, x=160, y=20)
splash.append(eeworld_sprite)

# Set text, font, and color
font = bitmap_font.load_font("/sd/resources/font/opposans_m_12.pcf")
# Create the text label
lable_area_fw = label.Label(
    font, x=10, y=120, text="Follow me 第四期!", scale=2, color=0xF8F8FF # 0x191970
)
lable_area_id = label.Label(
    font, x=20, y=160, text="ID.LODA", scale=2, color=(0, 191, 255)
)
splash.append(lable_area_fw)
splash.append(lable_area_id)

while True:
    time.sleep(0.5)
    led.value = not led.value
