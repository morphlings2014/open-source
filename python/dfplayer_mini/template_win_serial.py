# encoding=utf-8
import os
import cairosvg
import serial
import time
import DFRobotDFPlayerMini

DEMO_PLAYER_COM = "com16"
DEMO_PLAYER_BAUDRATE = 9600


def printDetail(type: int, value: int):
    if type == DFRobotDFPlayerMini.TimeOut:
        print("Time Out!")
    elif type == DFRobotDFPlayerMini.WrongStack:
        print("Stack Wrong!")
    elif type == DFRobotDFPlayerMini.DFPlayerCardInserted:
        print("Card Inserted!")
    elif type == DFRobotDFPlayerMini.DFPlayerCardRemoved:
        print("Card Removed!")
    elif type == DFRobotDFPlayerMini.DFPlayerCardOnline:
        print("Card Online!")
    elif type == DFRobotDFPlayerMini.DFPlayerUSBInserted:
        print("USB Inserted!")
    elif type == DFRobotDFPlayerMini.DFPlayerUSBRemoved:
        print("USB Removed!")
    elif type == DFRobotDFPlayerMini.DFPlayerPlayFinished:
        print("Number: " + value, end = ' ')
        print("Play Finished!")
    if type == DFRobotDFPlayerMini.DFPlayerError:
        print("DFPlayerError:", end = ' ')
        if value == DFRobotDFPlayerMini.Busy:
            print("Card not found")
        elif type == DFRobotDFPlayerMini.Sleeping:
            print("Sleeping")
        elif type == DFRobotDFPlayerMini.SerialWrongStack:
            print("Get Wrong Stack")
        elif type == DFRobotDFPlayerMini.CheckSumNotMatch:
            print("Check Sum Not Match")
        elif type == DFRobotDFPlayerMini.FileIndexOut:
            print("File Index Out of Bound")
        elif type == DFRobotDFPlayerMini.FileMismatch:
            print("Cannot Find File")
        elif type == DFRobotDFPlayerMini.Advertise:
            print("In Advertise")

def dfr_player_mini_setup(player: DFRobotDFPlayerMini.DFRPlayerMini):

    print("DFRobot DFPlayer Mini Demo")
    print("Initializing DFPlayer ... (May take 3~5 seconds)")

    # Use serial to communicate with mp3.
    if player.begin() != True:
        print("Unable to begin:")
        print("    1.Please recheck the connection!")
        print("    2.Please insert the SD card!")
        return False

    print("DFPlayer Mini online.")
    # Set volume value. From 0 to 30
    player.volume(10)
    # Play the first mp3
    player.play(1)
    return True

def dfr_player_mini_loop(player: DFRobotDFPlayerMini.DFRPlayerMini):
    timer = time.perf_counter()
  
    if (time.perf_counter() - timer > 3000):
        timer = time.perf_counter()
        # Play next mp3 every 3 second
        player.next()
  
    if (player.available()):
        # Print the detail message from DFPlayer to handle different errors and states.
        printDetail(player.readType(), player.read())

def dfr_test():
    com = serial.Serial(DEMO_PLAYER_COM, DEMO_PLAYER_BAUDRATE)
    player = DFRobotDFPlayerMini.DFRPlayerMini(com)
    if dfr_player_mini_setup(player):
        while True:
            dfr_player_mini_loop(player)

def tc_time():
    print('tc_time: ', end = ' ')
    print(time.perf_counter())

class tc_class:
    def __init__(self):
        self._sending = [0x7E, 0xFF, 0x06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0xEF]

    def _print_hex(self, bytes):
        l = [hex(int(i)) for i in bytes]
        print(" ".join(l))

    def _arrayToUint16(self, array):
        value = array[0] << 8 + array[1]
        return value

    def _uint16ToArray(self, value: int, array: list[int], index: int) -> None:
        array[index] = (value >> 8) & 0xFF
        array[index + 1] = value & 0xFF

    def _calculateCheckSum(self, buffer: list[int]) -> int:
        sum = 0
        for i in range(1, 7):
            sum += buffer[i]
        # revert
        return 0x10000 - sum

    def test(self):
        self._print_hex(self._sending)
        self._uint16ToArray(self._calculateCheckSum(tc_c._sending), self._sending, 7)
        self._print_hex(self._sending)

if __name__ == '__main__':
    # print(time.perf_counter())
    # tc_time()
    # time.sleep(1)
    # print(time.perf_counter())
    dfr_test()
    # tc_c = tc_class()
    # tc_c.test()
    # print(0x10002 & 0xFF)

# success_bytes = com.write('This is data for test')
# print success_bytes

# # 指定目录路径和输出文件路径
# directory_from = "from"
# directory_to = "to"
# output_file = "weather_ico.py"

# svg_path = 'from/100.svg'
# png_path = 'out/100.png'

# for filename in os.listdir(directory_from):
#         if filename.endswith('fill.svg'):
#             print(filename, end= ' ')
#         elif filename.endswith('.svg'):
#             svgpath = os.path.join(directory_from, filename)
#             pngpath = os.path.join(directory_to, os.path.splitext(filename)[0]) + '.png'
#             cairosvg.svg2png(url=svgpath, write_to=pngpath, scale=4)
#             # pixel_data = covert_img(filepath)
#             # variable_name = os.path.splitext(filename)[0]
#             # variable_value = 'ico_'+ variable_name

# _sending = [0x7E, 0xFF, 0x06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0xEF]
# _frame = [0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39]


# print(_frame)
# print(_frame[:2])
# print(_frame[1:])
